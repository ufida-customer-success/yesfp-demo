package demo.test.input_ticket;

import demo.entity.personal_ticket_holder.ReimburseCollection;
import demo.entity.input_ticket.StaBookBuildParam;
import demo.test.APITest;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @time 2021-05-13 15:21
 * 进项受票下的 报销台账Api测试入口类
 */
public class ReimburseLedgerTest {


    private static Logger LOGGER = LoggerFactory.getLogger(APITest.class);

    public static void main(String[] args) throws Exception {
        String result = "";

        preview();

        /**
         * 查验并缓存
         * */
        //result=verify();

        /**
         * 提交保存到报销台账  token从查验中获取
         * 发票保存token；1张发票对应1个saveToken。
         * 此token有效期为10分钟，超过时长此token失效，需要重新查验。token只能使用一次。
         * */
        //result=submit();
        /**
         * 报销台账发票上传返回全票面信息(PDF)
         */
        //result = V4_UPLOADPDF();

        /**
         * 纸票查验进报销台账
         */
        //result = VERIFY_AND_SUBMIT();

        /**
         * 不需要查验进入台账
         */
        //result = REIMBURSE_DIRECT_SAVE();



        /**
         * ocr识别接口v2
         * */
        //result=recognise();

        /**
         * 识别结果保存台账
         * */
        //result=OCR_Save();

        /**
         * 全票种不查验进台账
         */
        //result = direct_save();

        /**
         * 台账报销
         * */
        //result=reimbursed();

        /**
         * 台账取消报销
         * */
        //result=cancelReimbursed();

        /**
         * 台账记账
         * */
        //result=account();

        /**
         * 台账取消记账
         * */
        //result=cancelAccount();

        /**
         * 报销台账删除
         * */
        //result=delete();

        /**
         * 报销台账查询附件(全票种)
         */
        //result = VIEW_URL();

        /**
         * 其他发票台账查询
         */
        //result = OTHER();

        /**
         * 飞机票台账查询
         * */
        //result=air();

        /**
         *  火车票台账查询
         * */
        //result=train();

        /**
         * 出租车台账查询
         * */
        //result=taxi();

        /**
         * 机打发票台账查询
         * */
        //result=machine();
        /**
         * 定额发票台账查询
         * */
        //result=quota();
        /**
         * 过路费发票台账查询
         * */
        //result=tolls();
        /**
         * 客运汽车发票台账查询
         * */
        //result=passenger();
        /**
         *增值税发票台账查询
         * */
        //result=invoice();
        /**
         *报销台账查询详情信息接口
         *  */
        //result=detail();

        /**
         *报销台账置支付状态接口
         * */
        //result=paid();
        /**
         *报销台账置取消支付状态接口
         * */
        //result=canclePaid();
        /**
         * 报销台账更新凭证号接口
         * */
        //result=UPDATE_VOUCHERID();

        /**
         * 报销台账更新来源单据号接口
         */
        //result=UPDATE_SRCBILL();

        /**
         * 报销台账设置项目
         */
        //result=PROJECT_UPDATE();

        System.out.println(result);

    }

    //ocr识别后图片下载
    private static void preview() throws Exception {
        String imageId = "1626048329394446336";
        //json格式
        HttpClientUtil.getReturnByte(URLConfigEnum.PREVIEW_DOWNLOAD.getUrl()+"&imageId="+imageId);
    }

    //查验并缓存
    public static String verify() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.verfiy();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.VERIFY.getVerifyUrl(), paramsMap);
    }

    //从缓存提交保存到报销台账
    public static String submit() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.submit();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.SUBMIT.getUrl(), paramsMap);
    }

    /**
     * 报销台账发票上传返回全票面信息(PDF)
     * 支持pdf、ofd格式增值税发票，pdf格式通用电子发票到机打发票 (状态变更为报销中)
     */
    public static String V4_UPLOADPDF() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.V4_UPLOADPDF();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.V4_UPLOADPDF.getUrl(), paramsMap);
    }

    /**
     * 纸票查验进报销台账
     * @return
     * @throws Exception
     */
    public static String VERIFY_AND_SUBMIT() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.VERIFY_AND_SUBMIT();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.VERIFY_AND_SUBMIT.getVerifyUrl(), paramsMap);
    }

    /**
     * 不需要查验进入台账
     * @return
     */
    private static String REIMBURSE_DIRECT_SAVE() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.REIMBURSE_DIRECT_SAVE();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.REIMBURSE_DIRECT_SAVE.getUrl(), paramsMap);
    }


    /**
     *  OCR识别接口v2
     */
    public static String recognise() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.buildRecognisePostParam();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.RECOGNISE.getUrl(), paramsMap);
    }


    /**
     *  识别结果保存台帐
     */
    public static String OCR_Save() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.OCR_Save();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.OCR_SAVE.getUrl(), paramsMap);
    }

    /**
     * 全票种不查验进台账
     */
    public static String direct_save() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.direct_save();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.DIRECT_SAVE.getUrl(), paramsMap);
    }

    /**
     * 台账报销
     */
    public static String reimbursed() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.reimbursed();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.REIMBURSED.getUrl(), paramsMap);
    }

    /**
     * 台账取消报销
     */
    public static String cancelReimbursed() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.cancelReimbursed();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.CANCEL_REIMBURSED.getUrl(), paramsMap);
    }

    /**
     * 台账记账
     */
    public static String account() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.account();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.ACCOUNT.getUrl(), paramsMap);
    }

    /**
     * 台账取消记账
     */
    public static String cancelAccount() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.cancelAccount();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.CANCEL_ACCOUNT.getUrl(), paramsMap);
    }

    /**
     * 报销台账删除
     */
    public static String delete() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.delete();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.DELETE.getUrl(), paramsMap);
    }

    /**
     * 报销台账查询附件(全票种)
     */
    public static String VIEW_URL() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.VIEW_URL();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.VIEW_URL.getUrl(), paramsMap);
    }

    /**
     * 其他发票台账查询
     */
    public static String OTHER() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.OTHER();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.OTHER.getUrl(), paramsMap);
    }

    /**
     * 飞机票台账查询
     */
    public static String air() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.AIR.getUrl(), paramsMap);
    }

    /**
     * 火车票台账查询
     */
    public static String train() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.TRAIN.getUrl(), paramsMap);
    }

    /**
     * 出租车台账查询
     */
    public static String taxi() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.TAXI.getUrl(), paramsMap);
    }
    /**
     * 机打发票台账查询
     * */
    public static String machine() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.MACHINE.getUrl(), paramsMap);
    }
    /**
     * 定额发票台账查询
     * */
    public static String quota() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.QUOTA.getUrl(), paramsMap);
    }
    /**
     * 过路费发票台账查询
     * */
    public static String tolls() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.TOOLS.getUrl(), paramsMap);
    }
    /**
     * 客运汽车发票台账查询
     * */
    public static String passenger() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PASSENGER.getUrl(), paramsMap);
    }
    /**
     * 增值税台账查询
     */
    public static String invoice() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.find();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.INVOICE.getUrl(), paramsMap);
    }
    /**
     * 报销台账查询详情信息接口
     * */
    public static String detail() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.query();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.DETAIL.getUrl(), paramsMap);
    }
    /**
     *报销台账置支付状态接口
     * */
    public static String paid() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.paid();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PAID.getUrl(), paramsMap);
    }
    /**
     * 报销台账置取消支付状态接口
     */
    public static String canclePaid() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.canclePaid();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.CANCELPAID.getUrl(), paramsMap);
    }

    /**
     * 报销台账更新凭证号接口
     */
    public static String UPDATE_VOUCHERID() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.UPDATE_VOUCHERID();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.UPDATE_VOUCHERID.getUrl(), paramsMap);
    }

    /**
     * 报销台账更新来源单据号接口
     */
    public static String UPDATE_SRCBILL() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.UPDATE_SRCBILL();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.UPDATE_SRCBILL.getUrl(), paramsMap);
    }

    /**
     *报销台账设置项目
     */
    public static String PROJECT_UPDATE() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.PROJECT_UPDATE();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PROJECT_UPDATE.getUrl(), paramsMap);
    }

    //报销台账查询接口（新）
    public static String CollQuery() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.query();
        return HttpClientUtil.jsonPost(URLConfigEnum.QUERY.getUrl(), paramsMap);
    }


}

