package demo.test.input_ticket;

import demo.entity.unupload.input_ticket.InputTransferOutBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;
import java.util.Set;

/**
 * @author wrk
 * @date 2021/6/28 9:44
 * 进项转出接口
 * 1.1 采购台账新增进项转出
 * 1.2 报销台账新增进项转出
 * 1.3 删除转出明细
 */
public class InputTransferOut {
    public static void main(String[] args) throws Exception {
        String result="";
        //1.1 采购台账新增进项转出
//        result=purchaseOut() ;
        //1.2 报销台账新增进项转出
//        result=reimburseOut();
        //1.3 删除转出明细
//        result=delete();
        //1.4 新增手工转出
        result=outdetailAdd();
        System.out.println(result);
    }
    //1.1 采购台账新增进项转出
    private static String purchaseOut() throws Exception {
        Set paramsMap = InputTransferOutBuildParam.purchaseOut();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASEOUT.getUrl(), paramsMap);
    }
    //1.2 报销台账新增进项转出
    private static String reimburseOut() throws Exception {
        Set paramsMap = InputTransferOutBuildParam.reimburseOut();
        return HttpClientUtil.jsonPost(URLConfigEnum.REIMBURSEOUT.getUrl(), paramsMap);
    }
    //1.3 删除转出明细
    private static String delete() throws Exception {
        Set paramsMap = InputTransferOutBuildParam.delete();
        return HttpClientUtil.jsonPost(URLConfigEnum.OUTDETIALDELETE.getUrl(), paramsMap);
    }

    //1.4 新增手工转出
    private static String outdetailAdd() throws Exception {
        Map paramsMap = InputTransferOutBuildParam.outdetailAdd();
        return HttpClientUtil.jsonPost(URLConfigEnum.OUTDETAILADD.getUrl(), paramsMap);
    }
}
