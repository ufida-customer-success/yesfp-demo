package demo.test.input_ticket;

import demo.entity.input_ticket.DeductFormParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description: 进项-抵扣认证全流程
 * @Author: jiaogjin
 * @Date: 2022/5/17 15:59
 */
public class DeductFormTest {

    public static void main(String[] args) throws Exception {
        String result="";
        //查询当前税号税期
        result=period("201609140000001");
        //检测税盘是否在线
        //result=check("201609140000001");
        //从税务云获取专票（非税局，需要先到税务云同步数据）
        //result=queryInvoice();
        //发票勾选(抵扣+不抵扣)
        //result=gxtj();
        //撤销勾选(抵扣+不抵扣)
        //result=cxgx();
        //抵扣认证 - 统计
        //result=qrrz();
        //抵扣认证 - 查询
        //result=qrrzQuery();
        //抵扣认证 - 取消
        //result=qrrzCancel();
        System.out.println(result);
    }

    private static String period(String nsrsbh) throws Exception {
        System.out.println(URLConfigEnum.PERIOD.getUrl()+"&nsrsbh="+nsrsbh);
        return HttpClientUtil.get(URLConfigEnum.PERIOD.getUrl()+"&nsrsbh="+nsrsbh);
    }

    private static String check(String nsrsbh) throws Exception {
        System.out.println(URLConfigEnum.CHECK.getUrl()+"&nsrsbh="+nsrsbh);
        return HttpClientUtil.get(URLConfigEnum.CHECK.getUrl()+"&nsrsbh="+nsrsbh);
    }

    private static String queryInvoice() throws Exception {
        Map<String, Object> paramsMap = DeductFormParam.queryInvoice();
        return HttpClientUtil.jsonPost(URLConfigEnum.QUERYINVOICE.getUrl(),paramsMap);
    }

    private static String gxtj() throws Exception {
        Map<String, Object> paramsMap = DeductFormParam.gxtj();
        return HttpClientUtil.jsonPost(URLConfigEnum.GXTJ.getUrl(),paramsMap);
    }

    private static String cxgx() throws Exception {
        Map<String, Object> paramsMap = DeductFormParam.cxgx();
        return HttpClientUtil.jsonPost(URLConfigEnum.CXGX.getUrl(),paramsMap);
    }

    private static String qrrz() throws Exception {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("nsrsbh","91110000600001760P");
        paramsMap.put("password","税局抵扣认证密码");
        return HttpClientUtil.jsonPost(URLConfigEnum.QRRZ.getUrl(),paramsMap);
    }

    private static String qrrzQuery() throws Exception {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("nsrsbh","91110000600001760P");
        return HttpClientUtil.jsonPost(URLConfigEnum.QRRZQUERY.getUrl(),paramsMap);
    }

    private static String qrrzCancel() throws Exception {
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("nsrsbh","91110000600001760P");
        return HttpClientUtil.jsonPost(URLConfigEnum.QRRZCANCEL.getUrl(),paramsMap);
    }

}
