package demo.test.input_ticket;

import demo.entity.input_ticket.PurchaseParam;
import demo.entity.personal_ticket_holder.ReimburseCollection;
import demo.entity.input_ticket.StaBookBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @time 2021-05-13 15:22
 * 进程受票下的 采购台账API测试入口类
 */
public class PurchaseLedgerTest {
    public static void main(String[] args) throws Exception {
        String result="";
        //采购查验并缓存发票
        //result = PURCHASE_VERIFY();
        //从缓存保存到企业采购台账
        //result = PURCHASE_SAVE();
        //采购台账发票上传
        //result = uploadpdf();
        //采购台账不查验进入台账
        //result = directSave();
        //采购台账删除发票
        //result = PURCHASE_DELETE();
        //采购台账发票查询电票(此接口返回pdf)
        //result = EINVOICE_QUERY();
        //采购台账查询（新）
        //result = purchaseQuery();
        //采购台账入账  入账=记账
        //result = ACCOUNTSTATUS();
        //采购台账取消入账
        //result = CANCELACCOUNT();
        //采购台账结算
        //result = purchase();
        //采购台账取消结算
        //result = purchaseUnset();
        //采购台账附件查询
        //result = PURCHASE_VIEW_URL();
        //采购台账附件预览
        result = perview();
        //采购台账更新凭证号
        //result = updataVoucherid();
        //采购台账更新来源单据号
        //result = updataSrcbill();
        System.out.println(result);
    }

    //采购查验并缓存发票
    private static String PURCHASE_VERIFY() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_VERIFY();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASE_VERIFY.getVerifyUrl(), paramsMap);
    }

    //从缓存保存到企业采购台账
    private static String PURCHASE_SAVE() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_SAVE();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASE_SAVE.getUrl(), paramsMap);
    }

    //采购台账发票上传
    public static String uploadpdf() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.uploadpdf();
        return HttpClientUtil.jsonPost(URLConfigEnum.UPLOADPDF1.getUrl(), paramsMap);

    }

    //不需要查验进入台账
    //客户从第三方系统获得增值税发票全票面信息，不需要通过税务云再次查验获得全票面信息，直接通过接口传入台账
    private static String directSave() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.directSave();
        return HttpClientUtil.jsonPost(URLConfigEnum.DIRECTSAVE.getUrl(), paramsMap);
    }

    //采购台账删除发票
    private static String PURCHASE_DELETE() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_DELETE();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASE_DELETE.getUrl(), paramsMap);
    }

    //采购台账发票查询电票(此接口返回pdf)
    private static String EINVOICE_QUERY() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.EINVOICE_QUERY();
        return HttpClientUtil.jsonPost(URLConfigEnum.EINVOICE_QUERY.getUrl(), paramsMap);
    }

    //采购台账查询（新）
    private static String purchaseQuery() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.query();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASE_QUERY.getUrl(), paramsMap);
    }

    //采购台账入账 入账=记账
    private static String ACCOUNTSTATUS() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.ACCOUNTSTATUS();
        return HttpClientUtil.jsonPost(URLConfigEnum.ACCOUNTSTATUS.getUrl(), paramsMap);
    }

    //采购台账取消入账  更改购买发票状态为未记账状态
    private static String CANCELACCOUNT() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.CANCELACCOUNT();
        return HttpClientUtil.jsonPost(URLConfigEnum.CANCELACCOUNT.getUrl(), paramsMap);
    }

    //采购台账结算  结算不等于支付，结算后改变为已结算状态
    public static String purchase() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.purchase();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASEACCOUNT.getUrl(), paramsMap);
    }

    //采购台账取消结算
    public static String purchaseUnset() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.purchase();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASEUNSET.getUrl(), paramsMap);
    }

    //采购台账附件查询 返回页面为列表，操作查看可以进行下载（有效时间为30分钟）
    public static String PURCHASE_VIEW_URL() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.PURCHASE_VIEW_URL();
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASE_VIEW_URL.getUrl(), paramsMap);
    }

    //采购台账附件预览 只能看不能下载（有效时间为10分钟）
    private static String perview() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.perview();
        return HttpClientUtil.jsonPost(URLConfigEnum.INPUTPERVIEW.getUrl(), paramsMap);
    }
    //采购台账更新凭证号接口
    private static String updataVoucherid() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.updataVoucherid();
        return HttpClientUtil.jsonPost(URLConfigEnum.UPDATAVOUCHERID.getUrl(), paramsMap);
    }

    //采购台账更新来源单据号接口
    private static String updataSrcbill() throws Exception {
        Map<String, Object> paramsMap = PurchaseParam.updataSrcbill();
        return HttpClientUtil.jsonPost(URLConfigEnum.UPDATASRCBILL.getUrl(), paramsMap);
    }

}

