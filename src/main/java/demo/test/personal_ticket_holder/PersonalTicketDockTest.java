package demo.test.personal_ticket_holder;

import com.alibaba.fastjson.JSON;
import demo.entity.input_ticket.StaBookBuildParam;
import demo.entity.personal_ticket_holder.BackstageDocking;
import demo.utils.HttpClientUtil;
import demo.utils.MobileLoginUtils;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author wrk
 * @time 2021-05-13 16:32
 * 个人票夹后台对接api测试类
 */
public class PersonalTicketDockTest {
    public static void main(String[] args) throws Exception {
        String result = "";
        //绑定验证  可以运行
        //result=authCheck();
        //获取加密公钥
        //result=getPubKey();

        //账号登录并绑定
        /*String username = "17600104874";
        String password = "xiaobo0307hao";
        //登录获取token
        String token = MobileLoginUtils.loginV2(username,password);
        System.out.println(token);*/

        //token值每查询一次改变一次
        //票夹查询
        /*String username = "17600104874";
        String password = "xiaobo0307hao";
        testQuery(MobileLoginUtils.loginV2(username,password));*/

        //账号取消绑定
        result=unbind();
        System.out.println(result);
    }

    //账号绑定验证
    public static String authCheck() throws Exception {
        //json格式
        System.out.println(URLConfigEnum.AUTHCHECK.getUrl1("001"));
        Map<String, Object> entity = new HashMap<>();

        return HttpClientUtil.get(URLConfigEnum.AUTHCHECK.getUrl1("001"),entity);
    }

    //获取加密公钥
    private static String getPubKey() throws Exception {

        //json格式
        System.out.println(URLConfigEnum.PUBKEY.getNomrolUrl());
        return HttpClientUtil.get(URLConfigEnum.PUBKEY.getNomrolUrl());
    }

    //账号取消绑定
    private static String unbind() throws Exception {
        //json格式
        List paramsMap = BackstageDocking.unbind();
        return HttpClientUtil.jsonPost(URLConfigEnum.UNBIND.getUrl(),paramsMap);
    }


    //票夹查询
    public static void testQuery(String token) {

        //登录时获得的token
        String url = URLConfigEnum.ADVANCEQUERY.getNomrolUrl() + "?page=1&size=5&token="+token;
        Map<String, Object> entity = new HashMap<>();
        entity.put("fpjz", "1");
        entity.put("classify", "交通");
        entity.put("expensestatus", new Integer[] {-1,33,36});
        //entity.put("fpDm", "052002000211");
        //entity.put("fpHm", "24120644");

        String result = MobileLoginUtils.doPostJson(url, JSON.toJSONString(entity));

        System.out.println(result);

    }

}
