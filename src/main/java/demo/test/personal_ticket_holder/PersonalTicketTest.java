package demo.test.personal_ticket_holder;

import demo.entity.input_ticket.StaBookBuildParam;
import demo.entity.unupload.input_ticket.UserToOutputInvoiceBuildParam;
import demo.utils.Base64Util;
import demo.utils.FileUtils;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @time 2021-05-13 16:32
 * 个人全票种api测试类
 */
public class PersonalTicketTest {
    public static void main(String[] args) throws Exception {

        String result = "";
        /**
         * ocr识别接口
         * */
        //result = piaoedaRqrecognise();
        /**
         * ocr识别接口V2
         * */
        //result = piaoedaRqrecogniseV2();
        /**
         * ocr接口图片预览接口
         * */
        //piaoedaOcrPreview(1538703841043439616l);
        /**
         * 个人票夹列表查询
         * */
        result=billQuery();
        /**
         * 修改发票状态
         * */
        //result=purchaserStatus();
        /**
         * 根据号码代码获取信息
         * */
        //result=summar();
        /**
         * 个人票夹票据详情查询
         * */
        //result=detail();
        /**
         * 个人票价附件预览
         * */
       //result=billPerview();
        /**
         * 个人票夹附件下载
         * */
        //billDownload();
        /**
         *
         * 个人票夹新增
         * */
        //result=add();
        /**
         *
         * 个人票夹删除
         * */
        //result = billDelete();
        /**
         *
         * 个人票夹修改
         * */
        //result=billUpdate();
        /**
         * 个人票价提交到企业台账
         * */
        //result=commit();
        /**
         * 个人票夹行程单预览
         * */
        //result=preview();
        /**
         * 个人票夹行程单下载
         * */
        //result=download();
        /**
         * 个人票夹发票提交到采购台账
         */
        //result=fetch();
        System.out.println(result);
    }

    //ocr识别接口
    public static String piaoedaRqrecognise() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("usermobile","15011181852");
        paramsMap.put("useremail","");
        paramsMap.put("file", Base64Util.imageToBase64("D:\\常用文件\\税务云\\测试数据\\图片\\机打发票.jpg"));
        paramsMap.put("fileName","机打发票.jpg");
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PIAOEDARQRECOGNISE.getUrl(), paramsMap);
    }

    //OCR识别接口V2
    public static String piaoedaRqrecogniseV2() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = new HashMap<>();
        paramsMap.put("usermobile","18611143356");
        paramsMap.put("useremail","");
        paramsMap.put("file", Base64Util.imageToBase64("D:\\常用文件\\税务云\\测试数据\\图片\\增值税发票1.png"));
        paramsMap.put("fileName","增值税发票1.png");
        paramsMap.put("imageCut","1");
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PIAOEDARQRECOGNISEV2.getUrl(), paramsMap);
    }

    //Ocr接口图片预览接口
    public static void piaoedaOcrPreview(long imageId) throws Exception {
         HttpClientUtil.getReturnByte(URLConfigEnum.PIAOEDAOCRPREVIEW.getUrl()+"&imageId="+imageId);
    }

    //个人票夹列表查询
    public static String billQuery() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.buildInfo();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLQUERY.getUrl(), paramsMap);
    }

    //修改发票状态
    public static String purchaserStatus() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.changFapiaoStatus();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PURCHASERSTATUS.getUrl(), paramsMap);
    }

    //根据号码代码获取信息
    public static String summar() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.summar();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.SUMMARY.getUrl(), paramsMap);
    }

    //个人票夹票据详情查询
    public static String detail() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.detial();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLDETAIL.getUrl(), paramsMap);
    }

    //附件预览 以base64返回
    public static String billPerview() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.detial();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLPErVIEW.getUrl(), paramsMap);
    }

    //附件下载 以附件二进制流数据返回
    public static void billDownload() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = StaBookBuildParam.detial();
        //json格式
        HttpClientUtil.jsonPostReturnByte(URLConfigEnum.BILLDOWNLOAD.getUrl(), paramsMap);
    }

    //个人票夹新增
    private static String add() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.add();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLADD.getUrl(), paramsMap);
    }

    //个人票夹删除
    private static String billDelete() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.billDelete();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLDELETE.getUrl(), paramsMap);
    }

    //个人票夹修改
    private static String billUpdate() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.billUpdate();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.BILLUPDATE.getUrl(), paramsMap);
    }

    //个人票夹提交发票到报销台账_全票种
    public static String commit() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.commit();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.COMMIT.getUrl(), paramsMap);
    }

    //个人票夹行程单预览
    private static String preview() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.view();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.PREVIEW.getUrl(), paramsMap);
    }
    //个人票夹行程单下载
    private static String download() throws Exception {
        Map<String, Object> paramsMap = StaBookBuildParam.view();
        //json格式
        return HttpClientUtil.jsonPost(URLConfigEnum.DOWNLOAD.getUrl(), paramsMap);
    }

    //个人票夹发票提交到采购台账
    private static String fetch() throws Exception {
        Map<String, Object> paramsMap = UserToOutputInvoiceBuildParam.fetch();
        return HttpClientUtil.jsonPost(URLConfigEnum.FETCH.getUrl(), paramsMap);
    }

}

