package demo.test;

import demo.entity.output_invoice.InvoiceBuildParam;
import demo.entity.input_ticket.PurchaseParam;
import demo.entity.personal_ticket_holder.ReimburseCollection;
import demo.entity.input_ticket.StaBookBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * @program: yesfp-demo
 * @description: api的测试类
 * @author: kw
 * @create: 2020/05/21 15:22
 */
public class APITest {


    private static Logger LOGGER = LoggerFactory.getLogger(APITest.class);

    public static void main(String[] args) throws Exception {
        String result = "";
        //请求二维码信息
        //result =insertForQRInvoice();
        //专票红冲状态查询接口
        //result = redStateTotal();
        //个人用户同步
        //result = syncUser();
        //个人发票上传
        //result =uploadpdf();
        //发票取消上传
        // String result =CollDelete();
        //发票已报销
        //String result =CollReimbursed();
        //发票取消报销
        //String result =CollUnreimbursed();
        //入账
        //String result = accountStatus();
        //报销台账查询接口（新）
        // String result =CollQuery();
        //取消入账
        //String result =CollCancelAccount();
        //发票上传V2
        //String result =V2Uploadpdf();
        System.out.println(result);

    }

    //请求二维码信息
    /*private static String insertForQRInvoice() throws Exception {
        Map<String, Object> paramsMap = InvoiceBuildParam.insertForQRInvoice();
        return HttpClientUtil.get(URLConfigEnum.INSERT_FOR_QR_INVOICE.getUrl(), paramsMap);
    }*/

    //专票红冲状态查询接口
    private static String redStateTotal() throws Exception {
        Map<String, Object> paramsMap = InvoiceBuildParam.buildRedStateTotalParam();
        return HttpClientUtil.get(URLConfigEnum.RED_STATE_TOTAL.getUrl(), paramsMap);
    }

    //模拟回调服务
    //要求 1、post请求   2、服务公网可访问  3、接收请求传过来的参数
    /*
    @PostMapping(value = "getTicketInfo")
    public Object getTicketInfo(@RequestBody JSONObject v2) {
        System.out.println(v2);
        return v2;
    }

     */

    //个人用户同步
    public static String syncUser() throws Exception {
        Object paramMap = ReimburseCollection.syncUser();
        return HttpClientUtil.post(URLConfigEnum.SYNC_USER.getUrl(), paramMap);
    }

    //发票上传
    public static String uploadpdf() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.uploadpdf();
        return HttpClientUtil.jsonPost(URLConfigEnum.UPLOADPDF.getUrl(), paramsMap);

    }

    //发票取消上传
    public static String CollDelete() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.CollDelete();
        return HttpClientUtil.jsonPost(URLConfigEnum.COLLECTION_DELETE.getUrl(), paramsMap);

    }

    //发票已报销
    public static String CollReimbursed() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.CollReimbursed();
        return HttpClientUtil.jsonPost(URLConfigEnum.COLLECTION_REIMBURSED.getUrl(), paramsMap);

    }

    //发票取消报销
    public static String CollUnreimbursed() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.CollUnreimbursed();
        return HttpClientUtil.jsonPost(URLConfigEnum.COLLECTION_UNREIMBURSED.getUrl(), paramsMap);

    }

    //报销台账查询接口（新）
    public static String CollQuery() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.query();
        return HttpClientUtil.jsonPost(URLConfigEnum.QUERY.getUrl(), paramsMap);

    }
    //入账

    public static String accountStatus() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.accountStatus();
        return HttpClientUtil.jsonPost(URLConfigEnum.ACCOUNT_STATUS.getUrl(), paramsMap);

    }

    //取消入账
    public static String CollCancelAccount() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.CollCancelAccount();
        return HttpClientUtil.jsonPost(URLConfigEnum.COLLECTION_CANCEL_ACCOUNT.getUrl(), paramsMap);

    }

    //发票上传V2
    public static String V2Uploadpdf() throws Exception {
        Map<String, Object> paramsMap = ReimburseCollection.uploadpdf();
        return HttpClientUtil.jsonPost(URLConfigEnum.V2_UPLOADPDF.getUrl(), paramsMap);

    }
}
