package demo.test.output_invoice;

import demo.entity.output_invoice.InvoiceBuildParam;
import demo.entity.output_invoice.InvoiceWill;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @time 2021-05-13 15:14
 * 未开票收入管理api测试入口
 */
public class InvoiceWillTest {
    public static void main(String[] args) throws Exception {

        String result ="";
        //未开票查询 指的是待开票明细，不是未开票状态的发票
        //result=result();
        //新增未开票
        result=save();
        //未开票记录变更查询
        //result=change();
        System.out.println(result);

    }

    //未开票查询
    public static String result() throws Exception{
        Map<String, Object> paramsMap = InvoiceWill.result();
        return HttpClientUtil.jsonPost(URLConfigEnum.RESULT.getUrl(), paramsMap);
    }

    //新增未开票
    public static String save() throws Exception{
        Map<String, Object> paramsMap = InvoiceWill.save();
        return HttpClientUtil.jsonPost(URLConfigEnum.SAVE.getUrl(), paramsMap);
    }

    //未开票记录变更查询
    public static String change() throws Exception {
        //构造POST表单Map
        Map<String, Object> paramsMap = InvoiceWill.change();
        return HttpClientUtil.jsonPost(URLConfigEnum.CHANGE.getUrl(), (Map) paramsMap);
    }
}
