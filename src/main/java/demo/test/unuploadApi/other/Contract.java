package demo.test.unuploadApi.other;

import demo.entity.unupload.input_ticket.PaperTicketInspectionBuilderParam;
import demo.entity.unupload.other.ContractBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author wrk
 * @date 2021/5/24 13:47
 * 合同管理Api接口测试入口
 */
public class Contract {
    public static void main(String[] args) throws Exception {
        String result="";
        result=importContract();
//        result=changeContract();
        System.out.println(result);
    }
    //引入合同
    private static String importContract() throws Exception {
        List paramsMap = ContractBuildParam.importCon();
        return HttpClientUtil.jsonPost(URLConfigEnum.IMPORT.getUrl(), paramsMap);
    }

    //修改合同
    private static String changeContract() throws Exception {
        List paramsMap = ContractBuildParam.importCon();
        return HttpClientUtil.post(URLConfigEnum.CHANGECONTRACT.getUrl(), paramsMap);
    }
}
