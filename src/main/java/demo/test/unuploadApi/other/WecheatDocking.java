package demo.test.unuploadApi.other;

import demo.crypto.SignHelper;
import demo.entity.unupload.other.SaveInvoiceQueryParamBuild;
import demo.entity.unupload.other.WecheatDockingBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 10:46
 * 企业微信对接接口
 */
public class WecheatDocking {
    public static void main(String[] args) throws Exception {
        String result="";
        result=userinfo("yyy","241321", SignHelper.sign());
        System.out.println(result);
    }

    private static String userinfo(String code,String ts,String sign) throws Exception {
        Map paramsMap = WecheatDockingBuildParam.userinfo();
        return HttpClientUtil.get(URLConfigEnum.WXUSERINFO.getUrl()+"&code="+code+"&ts="+ts+"&sign="+sign);
    }
}
