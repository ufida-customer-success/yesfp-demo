package demo.test.unuploadApi.other;

import demo.entity.unupload.other.ContractBuildParam;
import demo.entity.unupload.other.SaveInvoiceQueryParamBuild;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 10:06
 * 销项信息查询
 */
public class SaveInvoiceQuery {
    public static void main(String[] args) throws Exception {
        String result="";
        result=query();
        System.out.println(result);
    }

    private static String query() throws Exception {
        Map paramsMap = SaveInvoiceQueryParamBuild.query();
        return HttpClientUtil.jsonPost(URLConfigEnum.SAVEINVOICEQUERY.getUrl(), paramsMap);
    }
}
