package demo.test.unuploadApi.other;

import demo.entity.personal_ticket_holder.ReimburseCollection;
import demo.entity.unupload.other.SynchronizationBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

/**
 * @author wrk
 * @date 2021/5/24 15:52
 * 用户同步接口
 */
public class Synchronization {
    public static void main(String[] args) throws Exception {
        String result="";
        //个人用户同步
//        result=syncUser();
        //用户组织关系同步
        result=syncOrc();
        System.out.println(result);
    }

    //用户组织关系同步
    public static String syncOrc() throws Exception {
        Object paramMap = SynchronizationBuildParam.syncOrg();
        return HttpClientUtil.jsonPost(URLConfigEnum.SYNC_USER_ORG.getUrl(), paramMap);
    }
    //个人用户同步
    public static String syncUser() throws Exception {
        Object paramMap = SynchronizationBuildParam.syncUser();
        return HttpClientUtil.jsonPost(URLConfigEnum.SYNC_USER.getUrl(), paramMap);
    }
}
