package demo.test.unuploadApi.other;

import demo.entity.unupload.other.ContractBuildParam;
import demo.entity.unupload.other.TaxRelatedProjectBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 14:35
 * 涉税项目管理
 */
public class TaxRelatedProject {
    public static void main(String[] args) throws Exception {
        String result="";
        //导入项目信息
        result=importProject();
        //修改项目信息
//        result=changeProject();
        //导入分包管理
//        result=importSubProject();
        //修改分包信息
//        result=changeSubProject();?
        System.out.println(result);
    }

    private static String importSubProject() throws Exception {
        List paramsMap = TaxRelatedProjectBuildParam.importSubPro();
        return HttpClientUtil.jsonPost(URLConfigEnum.IMPORTSUBPROJECT.getUrl(), paramsMap);
    }

    private static String changeSubProject() throws Exception {
        List paramsMap = TaxRelatedProjectBuildParam.importSubPro();
        return HttpClientUtil.jsonPost(URLConfigEnum.CHANGESUBPROJECT.getUrl(), paramsMap);
    }
    private static String importProject() throws Exception {
        List paramsMap = TaxRelatedProjectBuildParam.importPro();
        return HttpClientUtil.jsonPost(URLConfigEnum.IMPORTPROJECT.getUrl(), paramsMap);
    }
    private static String changeProject() throws Exception {
        List paramsMap = TaxRelatedProjectBuildParam.importPro();
        return HttpClientUtil.jsonPost(URLConfigEnum.CHANGEPROJECT.getUrl(), paramsMap);
    }
}
