package demo.test.unuploadApi.input_ticket;

import demo.entity.unupload.input_ticket.GetInvoiceBuildParam;
import demo.entity.unupload.input_ticket.PaperTicketInspectionBuilderParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 9:18
 * 纸票查验保存采购台
 */
public class PaperTicketInspection {
    public static void main(String[] args) throws Exception {
        String result="";
        //纸票查验保存采购台账
        result=verifyAndCommit();
        System.out.println(result);
    }

    private static String verifyAndCommit() throws Exception {
        Map<String, Object> paramsMap = PaperTicketInspectionBuilderParam.confirm();
        return HttpClientUtil.jsonPost(URLConfigEnum.VERIFYANDSUBMIT.getUrl(), paramsMap);
    }


}
