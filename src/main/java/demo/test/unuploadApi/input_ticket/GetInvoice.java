package demo.test.unuploadApi.input_ticket;

import demo.entity.input_ticket.PurchaseParam;
import demo.entity.unupload.input_ticket.GetInvoiceBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/21 16:28
 * 发票勾选（取消勾选）认证  openAPI
 */
public class GetInvoice {
    public static void main(String[] args) throws Exception {
        String result="";
        //专票查询
//        result=query();
        //发票勾选
        result=getInvoice();
        //发票取消勾选
//        result=delInvoice();
        //确认认证
//        result=confirm();
        System.out.println(result);
    }

    private static String confirm() throws Exception {
        Map<String, Object> paramsMap = GetInvoiceBuildParam.confirm();
        return HttpClientUtil.jsonPost(URLConfigEnum.QRRZ.getUrl(), paramsMap);
    }

    private static String getInvoice() throws Exception {
        Map<String, Object> paramsMap = GetInvoiceBuildParam.getInovice();
        return HttpClientUtil.jsonPost(URLConfigEnum.GXTJ.getUrl(), paramsMap);
    }
    private static String delInvoice() throws Exception {
        Map<String, Object> paramsMap = GetInvoiceBuildParam.getInovice();
        return HttpClientUtil.jsonPost(URLConfigEnum.CXGX.getUrl(), paramsMap);
    }

    private static String query() throws Exception {
        Map<String, Object> paramsMap = GetInvoiceBuildParam.query();
        return HttpClientUtil.jsonPost(URLConfigEnum.QUERYINVOICE.getUrl(), paramsMap);
    }

}
