package demo.test.unuploadApi.input_ticket;

import demo.entity.unupload.input_ticket.PaperTicketInspectionBuilderParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 9:18
 * 纸票查验保存报销台账
 */
public class PaperTicketToSalesAccount {
    public static void main(String[] args) throws Exception {
        String result="";
        //纸票查验保存报销台账404
        result=verifyAndCommit();
        System.out.println(result);
    }

    private static String verifyAndCommit() throws Exception {
        Map<String, Object> paramsMap = PaperTicketInspectionBuilderParam.confirm();
        return HttpClientUtil.jsonPost(URLConfigEnum.VERIFYANDSUBMITTOSALE.getUrl(), paramsMap);
    }


}
