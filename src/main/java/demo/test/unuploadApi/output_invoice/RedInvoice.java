package demo.test.unuploadApi.output_invoice;

import demo.entity.unupload.other.TaxRelatedProjectBuildParam;
import demo.entity.unupload.output_invoice.RedInvoiceBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 16:59
 * 专票红冲
 */
public class RedInvoice {
    public static void main(String[] args) throws Exception {
        String result="";
        result=redSpecial();
        System.out.println(result);
    }

    private static String redSpecial() throws Exception {
        Map<String, Object> paramsMap = RedInvoiceBuildParam.importSubPro();
        return HttpClientUtil.jsonPost(URLConfigEnum.SPECIALRED.getUrl(), paramsMap);
    }
}
