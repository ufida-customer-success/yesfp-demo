package demo.test.unuploadApi.output_invoice;

import demo.entity.unupload.input_ticket.PaperTicketInspectionBuilderParam;
import demo.entity.unupload.output_invoice.AutoReplenishmentBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 11:06
 * 自动补充接口
 */
public class AutoReplenishment {
    public static void main(String[] args) throws Exception {
        String result="";
        //得到appid对应的companyid  655b4f49-8740-11e7-8a67-70106fac13fa
        result=autoReplenishment("用友");
        //String companyid="09daa070-8740-11e7-8a67-70106fac13fa";
        //根据对于企业的companyid查找对应企业信息
        //result=getInfo(companyid);
        System.out.println(result);
    }

    private static String getInfo(String companyid) throws Exception {
        System.out.println(URLConfigEnum.GETINFO.getUrl()+"&companyid="+companyid);
        return HttpClientUtil.get(URLConfigEnum.GETINFO.getUrl()+"&companyid="+companyid);
    }

    private static String autoReplenishment(String name) throws Exception {
//        Map<String, Object> paramsMap = AutoReplenishmentBuildParam.confirm();
        System.out.println(URLConfigEnum.AUTOREPLEN.getUrl()+"&mc="+name);
        return HttpClientUtil.get(URLConfigEnum.AUTOREPLEN.getUrl()+"&mc="+name);
    }
}
