package demo.utils;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;

import demo.crypto.SignHelper;
import okhttp3.*;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * @author wrk
 * @date 2021/5/25 13:59
 */
public class HttpToCurlUtils {

    public static String HttpToCurl(String sign,String url,String canshu){
        return "curl -X POST -H \"sign:"+sign+"\" -H \"Content-Type:application/json; charset=utf-8\"  -H \"Connection:Keep-Alive\" -H \"Accept-Encoding:gzip\" -H \"User-Agent:okhttp/4.9.0\" -d '"+canshu +"' \""+url+"\"";
    }
    public static String HttpToCurlPost(String sign, String url){
        return "curl -X POST -H \"sign:"+sign+"\" -H \"Content-Type:application/x-www-form-urlencoded\"  -H \"Connection:Keep-Alive\" -H \"Accept-Encoding:gzip\" -H \"User-Agent:okhttp/4.9.0\" "+" \""+url+"\" -d '";
    }
    public static String HttpToCurlGet(String sign, String url){
        return "curl -X GET -H \"sign:"+sign+"\"  -H \"User-Agent:okhttp/4.9.0\" "+" \""+url+"\" ";
    }
}
