package demo.utils;

/**
 * @description:  url的配置类
 * @author: kw
 * @date: 2020/5/21
 * @param:
 * @return:
 */
public enum URLConfigEnum {

    //如果需要测试其他接口 直接在这里添加枚举即可

    //------------------------------------------------------------销项开票--------------------------------------------------------------

    //在税务云 openapi中 url为 /output-tax/api/invoiceApply/insertWithArray? 老的地址为invoiceclient-web，两者都可用，把 output-tax替换为invoiceclient-web即可
    //开票蓝票请求服务
    INSERT_WITH_ARRAY_URL("/output-tax/api/invoiceApply/insertWithArray?appid="),
    //开票蓝票请求服务--发票拆分
    INSERT_WITH_SPLIT ("/output-tax/api/invoiceApply/insertWithSplit?appid="),
    //开票状态查询服务
    QUERY_INBOICE_STATUS("/output-tax/api/invoiceApply/V3/queryInvoiceStatus?appid="),
    //发票红冲请求服务
    RED("/output-tax/api/invoiceApply/red?appid="),
    //开票申请审核通过
    ISSUE("/output-tax/api/invoiceApply/issue?appid="),
    //电子发票部分红冲
    PART_RED("/output-tax/api/invoiceApply/part-red?appid="),
    //删除开票失败申请
    DELETEINVOICEFAIL("/output-tax/api/invoiceApply/deleteInvoiceFailData?appid="),
    //邮件重发
    EMAILCALLBACK("/output-tax/api/invoiceApply/callBackByEmail?appid="),

    //开票申请删除
    INVOICE_APPLY_DEL_URL("/output-tax/api/invoiceApply/del?appid="),

    //发票打印
    TAX_PRINT("/output-tax/api/invoice-his/print?appid="),

    //扫码开票
    SCAN("/output-tax/api/invoiceApply/insertForQRInvoice?appid="),

    //申请红字信息表（购方销方）
    READINFOAPPLY("/output-tax/api/redinfo-apply/insertWithRedApply?appid="),
    //查询红字信息表参数
    QUERYREAINFO("/output-tax/api/redinfo-apply/queryRedInfoApply/"),

    //未开票查询
    RESULT("/output-tax/api/invoice-will/result?appid="),
    //未开票管理新增单据
    SAVE("/output-tax/api/invoice-will/save?appid="),
    //未开票记录变更查询
    CHANGE("/output-tax/api/invoice-will/changes?appid="),

    //发票作废
    INVALID("/output-tax/api/invoiceApply/invalid?appid="),



    //--------------------------------------------------------------个人票夹-------------------------------------------------------------

    //ocr识别接口
    PIAOEDARQRECOGNISE("/piaoeda-web/api/bill/ocr/rqrecognise?appid="),
    //OCR识别接口V2
    PIAOEDARQRECOGNISEV2("/piaoeda-web/api/bill/ocr/v2/rqrecognise?appid="),
    //Ocr接口图片预览接口
    PIAOEDAOCRPREVIEW("/piaoeda-web/api/bill/ocr/preview?appid="),
    //个人票夹列表查询
    BILLQUERY("/piaoeda-web/api/bill/query?page=1&size=10&appid="),
    //修改发票状态
    PURCHASERSTATUS("/piaoeda-web/api/bill/purchaser-status?appid="),
    //根据号码代码获取信息
    SUMMARY("/piaoeda-web/api/bill/summary?appid="),
    //个人票夹票据详情查询
    BILLDETAIL("/piaoeda-web/api/bill/detail?appid="),
    //个人票夹附件预览
    BILLPErVIEW("/piaoeda-web/api/bill/preview?appid="),
    //个人票夹附件下载
    BILLDOWNLOAD("/piaoeda-web/api/bill/download?appid="),
    //个人票夹新增
    BILLADD("/piaoeda-web/api/bill/add?appid="),
    //个人票夹删除
    BILLDELETE("/piaoeda-web/api/bill/delete?appid="),
    //个人票夹修改
    BILLUPDATE("/piaoeda-web/api/bill/update?appid="),
    //个人票夹提交发票到报销台账_全票种  提交至台账与V2接口一致 /piaoeda-web/api/bill/v2/commit
    COMMIT("/piaoeda-web/api/bill/commit?appid="),
    //个人票夹行程单预览
    PREVIEW("/piaoeda-web/api/bill/itinerary/preview?appid="),
    //个人票夹行程单下载
    DOWNLOAD("/piaoeda-web/api/bill/itinerary/download?appid="),
    //个人票夹发票提交到采购台账
    FETCH("/piaoeda-web/api/einvoice/v2/fetchPurchase?appid="),

    //账号绑定验证
    AUTHCHECK("/piaoeda-web/api/partner/v1/auth-check?appid="),
    //获取加密公钥
    PUBKEY("/cas/v1/pubkey"),
    //账号登录并绑定
    LOGIN("/cas/v1/mobile/user/mobileloginV2"),
    //票夹查询
    ADVANCEQUERY("/piaoeda-web/mobile/invoices/V2/advancequery"),
    //账号取消绑定
    UNBIND("/piaoeda-web/api/einvoice/v2/unbind-user?appid="),


    /**
     * 进项受票
     * */
    //-----------------------------------------------------报销台账非全票种-----------------------------------------------------------------

    //ocr识别后 查验并缓存
    VERIFY("/invoiceclient-web/api/reimburseCollection/ncc/verify_and_save?appid="),
    //从缓存提交保存到报销台账
    SUBMIT("/invoiceclient-web/api/reimburseCollection/ncc/submit?appid="),
    //报销台账发票上传返回全票面信息(PDF)
    V4_UPLOADPDF("/invoiceclient-web/api/reimburseCollection/v4/uploadpdf?appid="),
    //纸票查验进报销台账
    VERIFY_AND_SUBMIT("/invoiceclient-web/api/reimburseCollection/verify_and_submit?appid="),
    //不需要查验进入台账
    REIMBURSE_DIRECT_SAVE("/input-tax/api/reimburseCollection/direct-save?appid="),

    //-----------------------------------------------------报销台账全票种-----------------------------------------------------------------

    //OCR识别接口
    RECOGNISE("/input-tax/api/ocr/v2/recognise?appid="),
    //识别结果保存台帐
    OCR_SAVE("/input-tax/api/bill-collections/ocr-save?appid="),
    //全票种不查验进台账
    DIRECT_SAVE("/input-tax/api/bill-collections/direct-save?appid="),
    //台账报销
    REIMBURSED("/input-tax/api/bill-collections/reimbursed?appid="),
    //台账取消报销
    CANCEL_REIMBURSED("/input-tax/api/bill-collections/cancel-reimbursed?appid="),
    //台账记账
    ACCOUNT("/input-tax/api/bill-collections/account?appid="),
    //台账取消记账
    CANCEL_ACCOUNT("/input-tax/api/bill-collections/cancel-account?appid="),
    //报销台账删除
    DELETE("/input-tax/api/bill-collections/delete?appid="),
    //报销台账查询附件(全票种)
    VIEW_URL("/input-tax/api/reimburseCollection/V2/view-url?appid="),
    //其他发票台账查询
    OTHER("/input-tax/api/bill-collections/other?pagenum=1&pagesize=15&appid="),
    //飞机票台账查询
    AIR("/input-tax/api/bill-collections/air?pagenum=1&pagesize=15&appid="),
    //火车票台账查询
    TRAIN("/input-tax/api/bill-collections/train?pagenum=1&pagesize=15&appid="),
    //出租车台账查询
    TAXI("/input-tax/api/bill-collections/taxi?pagenum=1&pagesize=15&appid="),
    //机打发票台账查询
    MACHINE("/input-tax/api/bill-collections/machine?pagenum=1&pagesize=15&appid="),
    //定额发票台账查询
    QUOTA("/input-tax/api/bill-collections/quota?pagenum=1&pagesize=15&appid="),
    //过路费发票台账查询
    TOOLS("/input-tax/api/bill-collections/tolls?pagenum=1&pagesize=15&appid="),
    //客运汽车发票台账查询
    PASSENGER("/input-tax/api/bill-collections/passenger?pagenum=1&pagesize=15&appid="),
    //增值税台账查询
    INVOICE("/input-tax/api/bill-collections/reimburse?pagenum=1&pagesize=15&appid="),
    //报销台账查询详情信息接口
    DETAIL("/input-tax/api/bill-collections/detail?appid="),
    //报销台账置支付状态接口
    PAID ("/input-tax/api/bill-collections/paid?appid="),
    //报销台账置取消支付状态接口
    CANCELPAID ("/input-tax/api/bill-collections/cancel-paid?appid="),
    //报销台账更新凭证号接口
    UPDATE_VOUCHERID ("/input-tax/api/bill-collections/update-voucherid?appid="),
    //报销台账更新来源单据号接口
    UPDATE_SRCBILL ("/input-tax/api/bill-collections/update-srcbill?appid="),
    //报销台账设置项目
    PROJECT_UPDATE ("/input-tax/api/reimburseCollection/project/update?appid="),

    //-----------------------------------------------------采购台账-----------------------------------------------------------------

    //OCR识别发票 此接口和报销台账全票种中的OCR识别发票接口为同一接口

    //采购查验并缓存发票
    PURCHASE_VERIFY("/invoiceclient-web/api/purchase-collection/verify?appid="),
    //从缓存保存到企业采购台账
    PURCHASE_SAVE("/invoiceclient-web/api/purchase-collection/save?appid="),
    //采购台账发票上传
    UPLOADPDF1("/input-tax/api/einvoice/uploadpdf?appid="),
    //采购台账不需要查验进台账
    DIRECTSAVE("/input-tax/api/purchase-collection/direct-save?appid="),
    //采购台账删除发票
    PURCHASE_DELETE("/input-tax/api/einvoice/delete?appid="),
    //采购台账发票查询电票
    EINVOICE_QUERY("/input-tax/api/einvoice/query?appid="),
    //采购台账查询（新）
    PURCHASE_QUERY("/invoiceclient-web/api/purchase-collection/query?appid="),
    //采购台账入账
    ACCOUNTSTATUS("/input-tax/api/purchase-collection/accountStatus?appid="),
    //采购台账取消入账
    CANCELACCOUNT("/input-tax/api/purchase-collection/cancelAccount?appid="),
    //采购台账结算
    PURCHASEACCOUNT("/input-tax/api/purchase-account/settle?appid="),
    //采购台账取消结算
    PURCHASEUNSET("/input-tax/api/purchase-account/unsettle?appid="),
    //采购台账附件查询
    PURCHASE_VIEW_URL("/input-tax/api/purchase-collection/view-url?appid="),
    //采购台账附件预览 链接为临时链接，不是长期有效链接；
    INPUTPERVIEW("/input-tax/api/preview/link?appid="),
    //采购台账更新凭证号接口
    UPDATAVOUCHERID("/input-tax/api/purchase-collection/update-voucherid?appid="),
    //采购台账更新来源单据号接口
    UPDATASRCBILL("/input-tax/api/purchase-collection/ncc/update-srcbill?appid="),


    //-----------------------------------------------------进项-抵扣认证全流程-----------------------------------------------------------------

    //查询当前税号税期
    PERIOD("/invoiceclient-web/api/vat/period?appid="),
    //检测税盘是否在线
    CHECK("/invoiceclient-web/api/vat/disk/check?appid="),
    //从税务云获取专票（非税局，需要先到税务云同步数据）
    QUERYINVOICE("/invoiceclient-web/api/vat/queryInvoice?appid="),
    //发票勾选(抵扣+不抵扣)
    GXTJ("/invoiceclient-web/api/vat/gxtj?appid="),
    //撤销勾选(抵扣+不抵扣)
    CXGX("/invoiceclient-web/api/vat/cxgx?appid="),
    //抵扣认证 - 统计
    QRRZ("/invoiceclient-web/api/vat/qrrz?appid="),
    //抵扣认证 - 查询
    QRRZQUERY("/invoiceclient-web/api/vat/qrrz-query?appid="),
    //抵扣认证 - 取消
    QRRZCANCEL("/invoiceclient-web/api/vat/qrrz-cancel?appid="),


    //-----------------------------------------------------进项-进项转出-----------------------------------------------------------------

    //采购台账新增进项转出
    PURCHASEOUT("/input-tax/api/purchase-collection/rollout?appid="),
    //报销台账新增进项转出
    REIMBURSEOUT("/input-tax/api/reimburseCollection/rollout?appid="),
    //删除转出明细
    OUTDETIALDELETE("/input-tax/api/deduction/outdetail/delete?appid="),
    //新增手工转出
    OUTDETAILADD("/input-tax/api/deduction/outdetail/manu-rollout/add?appid="),


    //-----------------------------------------------------ofd能力接口-----------------------------------------------------------------

    //OFD渲染
    RENDER("/ofd/api/render?appid="),
    //ofd数据提取
    INVOCEDATA("/ofd/api/invoice-data?appid="),
    //OCR切割图片下载接口
    PREVIEW_DOWNLOAD("/piaoeda-web/api/bill/ocr/preview?appid="),


    //------------------------------------------------非标准openapi的接口，有部分接口不维护且和标准的接口功能是相同的-----------------------------------------------------------------------------------
    //请求二维码信息
    INSERT_FOR_QR_INVOICE("/invoiceclient-web/api/invoiceApply/insertForQRInvoice?appid="),
    //专票红冲状态查询接口
    RED_STATE_TOTAL("/output-tax/api/invoiceApply/red/state/total?appid="),

    //专票红冲(非标准api接口)
    SPECIALRED("/output-tax/api/invoiceApply/red/insert/total?appid="),

    //自动补充接口
    AUTOREPLEN("/taxinfo/api/auto-complete?appid="),
    GETINFO("/taxinfo/api/detail?appid="),

    //用户同步
    SYNC_USER("/piaoeda-web/api/einvoice/v2/sync-user?appid="),
    //用户组织关系同步
    SYNC_USER_ORG("/piaoeda-web/api/einvoice/v2/sync-user-orgs?appid="),

    //发票取消上传 线上api 由于版本变迁 由于不能确定用户用的那个url 为了保证用户的稳定 故 可能一个方法对应多个url 测试时  测试自己需要的url即可
    // 以下url都为发票取消上传 invoiceclient-web/api/einvoice/delete?appid=XXXXXX    input-tax/api/einvoice/delete?appid=XXXXXX
    COLLECTION_DELETE("/invoiceclient-web/api/reimburseCollection/delete?appid="),
    //发票已报销
    COLLECTION_REIMBURSED ("/invoiceclient-web/api/reimburseCollection/reimbursed?appid="),
    //发票取消报销
    COLLECTION_UNREIMBURSED("/invoiceclient-web/api/reimburseCollection/unreimbursed?appid="),
    //报销台账查询接口（新）
    QUERY("/invoiceclient-web/api/reimburseCollection/query?appid="),
    //入账
    ACCOUNT_STATUS("/invoiceclient-web/api/reimburseCollection/accountStatus?appid="),
    //取消入账
    COLLECTION_CANCEL_ACCOUNT("/invoiceclient-web/api/reimburseCollection/cancelAccount?appid="),
    //发票上传V2
    V2_UPLOADPDF("/input-tax/api/reimburseCollection/v2/uploadpdf?appid="),
    //发票上传
    UPLOADPDF("/invoiceclient-web/api/reimburseCollection/uploadpdf?appid="),

    //纸票查验进采购台账
    VERIFYANDSUBMIT("/invoiceclient-web/api/purchase-collection/verify_and_submit?appid="),
    //纸票查验进报销台账
    VERIFYANDSUBMITTOSALE("/input-tax/api/reimburseCollection/verify_and_submit?appid="),

    //未上传api  其他接口 url
    WXUSERINFO("/userinfo?appid="),
    //销项查询
    SAVEINVOICEQUERY("/invoiceclient-web/api/saleInvoiceCollection/query?appid="),
    //合同导入
    IMPORT("/bd/api/contract/import?appid="),
    //合同修改
    CHANGECONTRACT("/bd/api/contract/update?appid="),
    //项目导入
    IMPORTPROJECT("/building/api/project/import?appid="),
    //项目修改
    CHANGEPROJECT("/building/api/project/update?appid="),
    //导入分包管理
    IMPORTSUBPROJECT("/building/api/subproject/import?appid="),
    //分包修改
    CHANGESUBPROJECT("/building/api/subproject/update?appid="),

    //------------------------------------------------非标准openapi的接口---------------------------------------------------------------

    //------------------------------------------------证书，域名，密钥，appid---------------------------------------------------------------
    //pro22.pfx为测试环境通讯证书，正式环境需要替换成正式的
    //KEY_PATH("src/main/resources/certificate/陕西华洲置业发展有限公司.pfx"),
    KEY_PATH("src/main/resources/certificate/pro22.pfx"),
    //证书密码
    PASSWORD("password");
    //PASSWORD("4f3ec4");

    //测试环境有测试appid和证书，正式环境有正式appid和证书，请务必对应使用
    //测试环境appid就用这个，正式环境需要替换成正式的
    //private static String APPID = "097ad214-0841-4718-92af-bcec48716f59";
    private static String APPID = "commontesterCA";

    //这个是测试环境的域名，正式环境为https://fapiao.yonyoucloud.com
    //private static String DOMAIN = "https://tax.diwork.com"; //yonsuite yonbip 环境域名
    //private static String DOMAIN = "https://fapiao.yonyoucloud.com";  //税务云3.0 环境域名
    //private static String DOMAIN = "https://yonbip.diwork.com/yonbip-fi-taxgateway";
    private static String DOMAIN = "https://yesfp.yonyoucloud.com/";

    //------------------------------------------------证书，域名，密钥，appid---------------------------------------------------------------

    private String value;
    URLConfigEnum(String value) {
        this.value = value;
    }

    public String getValue() {return value;}


    /**
     * 包含appid的url
     * @return
     */
    public String getUrl() {
        return DOMAIN + value + APPID;
    }

    /**
     * 不包含appid的url
     * @return
     */
    public  String getNomrolUrl() {
        return DOMAIN + value ;
    }

    /**
     * url加上usercode
     * @return
     */
    public String getUrl1(String usercode) {
        return DOMAIN + value + APPID+"&usercode="+usercode;
    }

    /**
     * 拼接查验地址
     * @return
     */
    public String getVerifyUrl() {
        return DOMAIN + value + "commontesterCAVerifyAndOCR-202212";
    }

    /**
     *  查询红字信息表编号
     *  appid用下方的可查出数据
     * @param reqBillNo
     */
    public String queryRedInfo(String reqBillNo){
        return DOMAIN+value+reqBillNo+"?appid="+APPID;
    }
}
