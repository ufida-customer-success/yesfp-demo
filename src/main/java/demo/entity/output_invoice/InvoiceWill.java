package demo.entity.output_invoice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *未开票收入管理
 * */
public class InvoiceWill {
    /**
     * 未开票记录变更查询
     * */
    public static Map<String, Object> change() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();

        //发票号码
        paramsMap.put("beginTime","2022-07-25 16:31:15");
        paramsMap.put("endTime","2023-01-01 16:31:15");
        paramsMap.put("orgCode", "20160914001");
        paramsMap.put("pageNum", 1);
        paramsMap.put("pageSize", 15);

        return paramsMap;
    }

    /**
     * 构造 开票状态查询服务 表单数据
     */
    public static Map<String, String> buildQueryInvoiceStatusPostParam() {
        Map<String, String> paramsMap = new HashMap<String, String>();
        paramsMap.put("fpqqlsh", buildFpqqlsh());
        return paramsMap;
    }

    private static String buildFpqqlsh() {
        return "SX210402000030";
    }
    /**
     * 未开票查询
     */
    public static Map<String, Object> result() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //单据号
        //paramsMap.put("lyid", "");
        //单据请求流水号 ，单据请求流水号和单据号不能同时为空
        paramsMap.put("djqqlsh", "1522409425760706566");
        paramsMap.put("orgCode", "20160914001");
        paramsMap.put("pageNum", "1");
        paramsMap.put("pageSize", "10");

        return paramsMap;
    }

    //新增未开票
    public static Map<String, Object> save() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //单据号
        paramsMap.put("lyid", "12423423123");
        //来源单据号
        paramsMap.put("lydjh", "123423");
        //单据请求流水号 ，单据请求流水号和单据号不能同时为空
        paramsMap.put("djqqlsh", "2377039923423");
        paramsMap.put("orgCode", "20160914001");
        paramsMap.put("GMF_MC", "广州佰仕德材料科技有限公司");
        paramsMap.put("GMF_NSRSBH", "91440101MA5CR3FU35");
        paramsMap.put("GMF_DZDH", "广州市花都区红棉大道北16号4楼4B07室 020-61796191");
        paramsMap.put("GMF_YHZH", "中国农业银行股份有限公司广州花都名门支行 44087001040011474");

        paramsMap.put("zdrq","2022-07-26");
        paramsMap.put("invoiceWillBs",buildWillbs());
        return paramsMap;
    }

    private static Object buildWillbs() {

        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("XMMC", "硅胶5299B");
        paramsMap.put("XMBM", "202020012");
        paramsMap.put("GGXH", "25kg/桶");
        paramsMap.put("DW", "千克");
        paramsMap.put("XMSL", 2);
        paramsMap.put("XMHSDJ", 20);
        paramsMap.put("XMJSHJ", 40);
        paramsMap.put("hh","1");
        paramsMap.put("SPBM", "1070213070000000000");
        paramsMap.put("SL", 0.16);
        return paramsMap;
    }
    //新增红字信息表
    public static Map<String, Object> apply() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("reqMemo", "1100000000");
        paramsMap.put("gmfMc", "接口测试组织");
        paramsMap.put("gmfNsrsbh", "201609140000001");
        paramsMap.put("xsfNsrsbh", "201609140000001");//91110105MA0084MW37
        paramsMap.put("xsfMc", "测试333");
        //红字申请表成品油涉及类型  1：涉及销售数量变更，2：涉及销售金额变更
        //paramsMap.put("cpyMemo", "");
        paramsMap.put("kprq","202101");
        paramsMap.put("items",buildRedApplyItems());
        paramsMap.put("hjje",-86.21);
        paramsMap.put("hjse",-13.79);
        paramsMap.put("jshj",-100);
        paramsMap.put("reqBillNo","3211ss232123213");
        paramsMap.put("xmmc","带上简码的项目名称");
        paramsMap.put("ysxmmc","不带简码的项目名称");
        paramsMap.put("urls","http://47x826661y.qicp.vip/kaipiao/callback");
        return paramsMap;
    }

    /**
     * 申请红字信息表明细 构造参数
     */
    private static Object buildRedApplyItems() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> data2 = new HashMap<String, Object>();
        Map<String, Object> data1 = new HashMap<String, Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("xmjshj", -10);
        data.put("xmje", -8.62);
        data.put("XMMC", "POLO衫");
        data.put("XMBM", "202020012");
        data.put("GGXH", "25kg/桶");
        data.put("DW", "千克");
        data.put("XMSL", 2);
        data.put("se", -1.38);
        //税率16%需要写成0.16的格式
        data.put("SL", 0.16);
        //SPBM字段为商品税收分类编码，不同的商品会有不同的编码，不对应的话会影响报税，需要咨询下公司财务
        data.put("SPBM", "1090625010000000000");
        //items.add(data);

        data1.put("xmjshj", -100);
        data1.put("xmje", -86.21);
        data1.put("XMMC", "POLO衫");
        data1.put("XMBM", "202020012");
        data1.put("GGXH", "25kg/桶");
        data1.put("DW", "千克");
        data1.put("XMSL", -2);
        data1.put("se", -13.79);
        //税率16%需要写成0.16的格式
        data1.put("SL", 0.16);
        //SPBM字段为商品税收分类编码，不同的商品会有不同的编码，不对应的话会影响报税，需要咨询下公司财务
        data1.put("SPBM", "1090625010000000000");
        items.add(data1);

        data2.put("xmjshj", 10);
        data2.put("xmje", 8.85);
        data2.put("XMMC", "饮用水 娃哈哈 Wahaha");
        data2.put("XMBM", "1316927134524661760");
        data2.put("GGXH", "596ml");
        data2.put("DW", "箱");
        data2.put("XMSL", 2);
        data2.put("SE", 1.15);
        //税率16%需要写成0.16的格式
        //data2.put("SL", 0.13);
        //SPBM字段为商品税收分类编码，不同的商品会有不同的编码，不对应的话会影响报税，需要咨询下公司财务
        //data2.put("SPBM", "3060101000000000000");
        //items.add(data2);
        return items;
    }

    /**
     * 构造request发票明细
     */
    private static List<Object> buildItems() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> data1 = new HashMap<String, Object>();

        data1.put("XMJSHJ", -100);
        data1.put("YSXMMC", "橡皮");
        data1.put("XMMC", "*运输服务*橡皮");
        data1.put("xmdj","23.0041942911");
        data1.put("xmhsdj","23.234213");
        data1.put("GGXH", "一二三四五六七八");
        data1.put("DW", "一二三");
        data1.put("XMSL", "-4.3039977296");
        data1.put("SE", -0.99);
        //税率16%需要写成0.16的格式
        data1.put("SL", 0.01);
        data1.put("xmje",-99.01);
        data1.put("spbm", "3213124314324322");
        items.add(data1);

        return items;
    }
}
