package demo.entity.personal_ticket_holder;

import com.google.gson.GsonBuilder;
import demo.utils.Base64Util;

import java.util.*;

/**
 * 个人票价后台对接
 * */
public class BackstageDocking {


    public static List unbind() {
        List paramsMap = new ArrayList();
        paramsMap.add( buildRecognisePostParam());
        return paramsMap;
    }

        public static Map<String, Object> buildRecognisePostParam() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //        "mobile": "13888888888",
        //        "email": "aaa@a.com",
        //    "partnerUser":"XXXX"
        paramsMap.put("mobile", "13888888888");
        paramsMap.put("email", "aaa@a.com");
        paramsMap.put("partnerUser", "XXXX");
        return paramsMap;
    }

    private static String buildUrlConfigs() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("url", "http://18z7873y70.eicp.vip/taxcloud/callback");
        datas.add(data);
        GsonBuilder builder = new GsonBuilder();
        return builder.create().toJson(datas);
    }
}
