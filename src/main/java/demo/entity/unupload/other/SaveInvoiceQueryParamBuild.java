package demo.entity.unupload.other;

import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 10:06
 * 销项信息查询
 */
public class SaveInvoiceQueryParamBuild {
    public static Map<String, Object> query() {
        //{

        //    "zfbz":"N",
        //    "gmfNsrsbh": "购买方纳税人识别号", 选填
        //    "xsfNsrsbh":"销售方纳税人识别号", 选填
        //}
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("fpHm", "");
        paramsMap.put("fpDm", "");
        paramsMap.put("zfbz","N");
        paramsMap.put("kprq_begin", "20190101");
        paramsMap.put("kprq_end", "20220202");
        paramsMap.put("fplx", "1");
        paramsMap.put("hasItems", "true");
        paramsMap.put("kplx", "0");
        paramsMap.put("page",1);
        paramsMap.put("size",5);
        return paramsMap;
    }
}
