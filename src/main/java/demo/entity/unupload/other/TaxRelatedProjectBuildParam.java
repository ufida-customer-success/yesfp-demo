package demo.entity.unupload.other;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 14:35
 * 涉税项目管理
 */
public class TaxRelatedProjectBuildParam {
    public static List importPro() {
        List list=new ArrayList();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("addressAndPhone", "CH 19811118833");
        paramsMap.put("bankAndAccount", "建行 2198318393823213123");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("contractCodes", buildCodes());
        paramsMap.put("progress", "20");
        paramsMap.put("projectCode", "12333");
        paramsMap.put("projectName", "合同01");
        paramsMap.put("customerNsrsbh", "201609140000001");
        paramsMap.put("relativeName", "合同01");
        paramsMap.put("taxMethod", "1");
        paramsMap.put("taxRelatedType", "1");
        list.add(paramsMap);
        return list;
    }
    public static List importSubPro() {
        //  {  {
        //    "supplierName": "分包企业名称",
        //    "supplierNsrsbh": "分包企业税号"
        //  }
        List list=new ArrayList();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("contractCodes", buildCodes());
        paramsMap.put("subprojectCode", "122");
        paramsMap.put("projectName", "合同01");
        paramsMap.put("subprojectName", "合同01");
        paramsMap.put("supplierName", "测试");
        paramsMap.put("supplierNsrsbh", "201609140000001");
        list.add(paramsMap);
        return list;
    }

    private static List<Object> buildCodes() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("code","ht00111");
        datas.add(paramsMap);
        return datas;
    }
}
