package demo.entity.unupload.other;

import demo.entity.unupload.input_ticket.PaperTicketInspectionBuilderParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 13:47
 * 合同管理Api接口测试入口
 */
public class ContractBuildParam {

    public static List importCon() {
        List list=new ArrayList();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("amount", 0);
        paramsMap.put("code", "12211233");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("isFrame", "Y");
        paramsMap.put("contractStatus", "1");
        paramsMap.put("contractType", "1");
        paramsMap.put("name", "合同01");
        paramsMap.put("note", "11");
        paramsMap.put("paymentType", "1");
        paramsMap.put("relativeNsrsbh", "201609140000001");
        paramsMap.put("relativeName", "合同01");
        paramsMap.put("signDate", "2019-09-09");
        paramsMap.put("validDate", "2019-09-09");
        list.add(paramsMap);
        return list;
    }
}
