package demo.entity.unupload.other;

import demo.crypto.SignHelper;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 10:46
 * 企业微信对接接口
 */
public class WecheatDockingBuildParam {
    public static Map<String, Object> userinfo() throws Exception {
        //{

        //    "zfbz":"N",
        //    "gmfNsrsbh": "购买方纳税人识别号", 选填
        //    "xsfNsrsbh":"销售方纳税人识别号", 选填
        //}
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("code", "yyy");
        paramsMap.put("sign", SignHelper.sign()+"");
        paramsMap.put("ts", "241321");
        return paramsMap;
    }
}
