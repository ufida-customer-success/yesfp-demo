package demo.entity.unupload.other;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 15:52
 * 用户同步接口
 */
public class SynchronizationBuildParam {
    //个人用户同步
    public static Object syncUser() {
        List<Object> paramsList = new ArrayList<Object>();
        Map<String, Object> user = new HashMap<String, Object>();
        user.put("username","张三");
        user.put("mobile","19831139387");
        user.put("userMobile","userMobile");
        user.put("email","aaass@a.com");
        user.put("status",1);

        Map<String, Object> orgRelations = new HashMap<String, Object>();
        paramsList.add(user);
        return paramsList;
    }
    //用户组织关系同步
    public static Object syncOrg() {
        List<Object> paramsList = new ArrayList<Object>();
        Map<String, Object> user = new HashMap<String, Object>();
        user.put("userMobile","19831139387");
        user.put("email","aaass@a.com");
        user.put("orgRelations",buildOrg());

        Map<String, Object> orgRelations = new HashMap<String, Object>();
        paramsList.add(user);
        return paramsList;
    }

    private static Map buildOrg() {
        Map map=new HashMap();
        map.put("orgCode","20160914001");
        map.put("status",2);
        map.put("authOpt","3");
        return map;
    }
}
