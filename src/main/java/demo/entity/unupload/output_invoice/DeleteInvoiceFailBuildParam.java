package demo.entity.unupload.output_invoice;

import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 11:17
 * 删除开票失败申请
 */
public class DeleteInvoiceFailBuildParam {
    public static Map<String, Object> delete() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("fpqqlsh", "1551760652516483072");
        return paramsMap;
    }

}
