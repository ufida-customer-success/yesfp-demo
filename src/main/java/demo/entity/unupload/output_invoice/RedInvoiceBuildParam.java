package demo.entity.unupload.output_invoice;

import demo.entity.unupload.other.TaxRelatedProjectBuildParam;
import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 16:59
 * 专票红冲
 */
public class RedInvoiceBuildParam {
    public static Map<String, Object> importSubPro() {
        // "yfpDm":"485051752143",    "yfpHm":"00671503"
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("yfpDm", "111005056586");
        paramsMap.put("yfpHm", "36338450");
        return paramsMap;
    }
}
