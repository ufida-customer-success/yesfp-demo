package demo.entity.unupload.output_invoice;

import java.util.HashMap;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 9:41
 * 邮件重发
 */
public class EmailCallBackParamBuild {
    public static Map<String, Object> emaillCallBack() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("fpqqlsh", "20220512190258987");
        paramsMap.put("email", "jiaogjin@yonyou.com");
        return paramsMap;
    }
}
