package demo.entity.unupload.input_ticket;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author wrk
 * @date 2021/6/28 9:44
 * 进项转出接口
 * 1.1 采购台账新增进项转出
 * 1.2 报销台账新增进项转出
 * 1.3 删除转出明细
 */
public class InputTransferOutBuildParam {

    //1.1 采购台账新增进项转出
    public static Set purchaseOut() {
        //    "fpDm": "5100172130",
        //    "fpHm": "08946106",
        //    "outDate": "2018-07-20",
        //    "operator": "操作员",
        //    "outMoney": 77.77,
        //    "reason": 2,
        //    "vnote": "备注"
        Set set=new HashSet();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("fpDm", "044007232066");
        paramsMap.put("fpHm", "45782159");
        paramsMap.put("outDate", "2021-06-28");
        paramsMap.put("operator", "kw");
        paramsMap.put("outMoney", "10");
        paramsMap.put("reason", "2");
        paramsMap.put("vnote", "test");
        paramsMap.put("bz", "test1");
        set.add(paramsMap);
        return set;
    }
    //1.2 报销台账新增进项转出
    public static Set reimburseOut() {
        Set set=new HashSet();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("fpDm", "1100203130");
        paramsMap.put("fpHm", "20765159");
        paramsMap.put("outDate", "2021-06-28");
        paramsMap.put("operator", "kw");
        paramsMap.put("outMoney", "50");
        paramsMap.put("reason", "2");
        paramsMap.put("vnote", "test");
        set.add(paramsMap);
        return set;
    }
    //1.3 删除转出明细
    public static Set delete() {
        Set set=new HashSet();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("fpDm", "044007232066");
        paramsMap.put("fpHm", "45782159");
        set.add(paramsMap);
        return set;
    }

    public static Map outdetailAdd() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("outDatetime", "2018-07-20");
        paramsMap.put("nsrsbh", "101011111111111");
        paramsMap.put("orgcode", "45782159");
        paramsMap.put("outMoney", 77.77);
        paramsMap.put("reason", 2);
        paramsMap.put("voucherId", "凭证号");
        paramsMap.put("vnote", "备注");
        return  paramsMap;
    }
}
