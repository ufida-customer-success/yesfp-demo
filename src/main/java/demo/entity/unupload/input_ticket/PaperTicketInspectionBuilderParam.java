package demo.entity.unupload.input_ticket;

import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/24 9:18
 * 纸票查验保存采购台
 */
public class PaperTicketInspectionBuilderParam {


    //确认认证
    public static Map<String, Object> confirm() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "kw");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("invoices", buildinvoices());
        return paramsMap;
    }
    private static Object buildinvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje",1395.28);
        data.put("jshj",1479.00);
        data.put("fpDm","3100201130");
        data.put("kprq","20210421");
        data.put("fpHm","51143619");
        data.put("jym","162191");
        data.put("srcBillType","invoice");
        data.put("srcBillCode", "51143619");
        datas.add(data);
        return datas;
    }

}
