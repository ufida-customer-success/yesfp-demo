package demo.entity.unupload.input_ticket;

import demo.utils.HttpClientUtil;
import demo.utils.URLConfigEnum;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/26 16:13
 * 个人票夹发票提交到采购台账
 */
public class UserToOutputInvoiceBuildParam {
    public static Map<String, Object> fetch() {
        // "yfpDm":"485051752143",    "yfpHm":"00671503"
        List list=new ArrayList();
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("srcBillCode", "51143619");
        paramsMap.put("invoices",buildinvoices());
        list.add(paramsMap);
        return paramsMap;
    }
    private static Object buildinvoices() {
        //
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje",1395.28);
        data.put("jshj",1479.00);
        data.put("fpDm","3100201130");
        data.put("kprq","20210421");
        data.put("fpHm","51143619");
        data.put("usercode","15011181852");
        data.put("jym","162191");
        data.put("srcBillType","invoice");
        data.put("srcBillCode", "51143619");
        datas.add(data);
        return datas;
    }
}
