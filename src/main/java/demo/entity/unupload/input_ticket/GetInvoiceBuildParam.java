package demo.entity.unupload.input_ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wrk
 * @date 2021/5/21 16:28
 * 发票勾选（取消勾选）认证  entity
 */
public class GetInvoiceBuildParam {
    public static Map<String, Object> query() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //  "rq_q":"yyyy-MM-dd",
        //  "rq_z":"yyyy-MM-dd",
        //  "period":"",
        //  "nsrsbh":""，
        //  "orgcode":"",
        paramsMap.put("rq_q", "2018-10-10");
        paramsMap.put("rq_z", "2022-10-10");
        paramsMap.put("period", "202002");
        paramsMap.put("orgcode", "20160914001");
        return paramsMap;
    }
    //确认认证
    public static Map<String, Object> confirm() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        //  "rq_q":"yyyy-MM-dd",
        //  "rq_z":"yyyy-MM-dd",
        //  "period":"",
        //  "nsrsbh":""，
        //  "orgcode":"",
        paramsMap.put("nsrsbh", "201609140000001");
        return paramsMap;
    }
    public static Map<String, Object> getInovice() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("hmbms",buildHmbms());
        paramsMap.put("nsrsbh","201609140000001");
//        paramsMap.put("orgcode", "20160914001");
        return paramsMap;
    }
    public static Map<String, Object> DeleteInovice() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("hmbms",buildHmbms());
        paramsMap.put("nsrsbh","201609140000001");
//        paramsMap.put("orgcode", "20160914001");
        return paramsMap;
    }
    public static List<Object> buildHmbms(){
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        //发票代码            "fpDm": "3100111130",
        //            "fpHm": "11404542",
        //            "kprq": "20170831"
        data.put("fpDm","94285430");
        //发票号码
        data.put("fpHm","122001107700");
        data.put("kprq","20210521");
        datas.add(data);
        return datas;
    }
}
