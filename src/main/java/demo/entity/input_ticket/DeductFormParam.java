package demo.entity.input_ticket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:
 * @Author: jiaogjin
 * @Date: 2022/5/17 16:27
 */
public class DeductFormParam {

    public static Map<String, Object> queryInvoice() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();

        //发票号码
        paramsMap.put("fpdm","");
        paramsMap.put("fphm","");
        List<Map<String, Object>> hmDms = new ArrayList<>();
        Map<String, Object> hmDm = new HashMap<String, Object>();
        hmDm.put("fpdm","");
        hmDm.put("fphm","");
        hmDms.add(hmDm);
        paramsMap.put("hmDms",hmDms);
        paramsMap.put("rq_q", "2019-10-24");
        paramsMap.put("rq_z", "2021-01-01");
        paramsMap.put("xsfnsrsbh", "201609140000001");
        //paramsMap.put("fpzt","" );
        //paramsMap.put("fplx","" );
        //paramsMap.put("rzzt", "");
        //paramsMap.put("period", "");
        paramsMap.put("nsrsbh","201609140000001" );
        //paramsMap.put("orgcode","" );
        paramsMap.put("page", 0);
        paramsMap.put("size", 50);
        return paramsMap;
    }

    public static Map<String, Object> gxtj() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        List<Map<String, Object>> hmDms = new ArrayList<>();
        Map<String, Object> hmDm = new HashMap<String, Object>();
        hmDm.put("fpdm","3100111130");
        hmDm.put("fphm","11404542");
        hmDm.put("kprq","20170831");
        hmDm.put("yxse","23");
        hmDms.add(hmDm);
        paramsMap.put("deductMode", 1);
        paramsMap.put("mobileOrEmail","" );
        paramsMap.put("nsrsbh","201609140000001" );
        paramsMap.put("orgcode","" );
        return paramsMap;
    }

    public static Map<String, Object> cxgx() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        List<Map<String, Object>> hmDms = new ArrayList<>();
        Map<String, Object> hmDm = new HashMap<String, Object>();
        hmDm.put("fpdm","3100171130");
        hmDm.put("fphm","11404542");
        hmDm.put("kprq","20170831");
        hmDms.add(hmDm);
        paramsMap.put("deductMode", 1);
        paramsMap.put("nsrsbh","201609140000001" );
        paramsMap.put("orgcode","" );
        return paramsMap;

    }
}
