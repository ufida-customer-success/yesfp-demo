package demo.entity.input_ticket;

import demo.utils.Base64Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 台账
 */
public class StaBookBuildParam {
    /**
     * OCR识别
     * @return
     */
    public static Map<String, Object> buildRecognisePostParam() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
      //  paramsMap.put("nsrsbh", "91110105MA0084MW37");
        paramsMap.put("orgcode", "20160914001");
      //  paramsMap.put("orgcode", "91110105MA0084MW37");
        //注意 base64编码不能有换行  选择BASE64Encoder需要将换行处理  用Base64比较好
        System.out.println(Base64Util.imageToBase64("C:\\Users\\Administrator\\Desktop\\大发票.jpg"));
        paramsMap.put("file", Base64Util.imageToBase64("C:\\Users\\Administrator\\Desktop\\大发票.jpg"));
        //paramsMap.put("file", Base64Util.NetImageToBase64("https://ss0.bdstatic.com/94oJfD_bAAcT8t7mm9GUKT-xh_/timg?image&quality=100&size=b4000_4000&sec=1590057248&di=a298d5f9728d9ded45906a643d8b8b2d&src=http://5b0988e595225.cdn.sohucs.com/images/20180810/75d14550cff44cf4bcc0346dd50c3aae.jpeg"));//ImageToBase64("D:\\1.png"));
        return paramsMap;
    }
    //查验并缓存
    public static Map<String, Object> verfiy() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "kw");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("invoices", buildinvoices());
        return paramsMap;
    }
    //保存
    public static Map<String, Object> submit() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "kw");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("invoices", buildSaveinvoices());
        return paramsMap;
    }
    /**
     * 识别结果保存台帐
     */
    public static Map<String,Object> OCR_Save(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",OCR_SaveBills());

        return paramsMap;
    }


    /**
     * 保存报销票据明细
     */
    public static List<Object> OCR_SaveBills(){
        List<Object> bills = new ArrayList<Object>();
        Map<String, Object> datas = new HashMap<String, Object>();
        datas.put("imageId","60214");
        datas.put("billType","train");
        datas.put("data", OCR_SaveTrainData());
        bills.add(datas);
        return bills;
    }
    /**
     * 机打发票data
     */
    public static Map<String,Object> OCR_SaveMachineData(){
        Map<String, Object> data = new HashMap<String, Object>();
        //开票日期"yyyyMMdd"
        data.put("date","20200610");
        //消费类型
        data.put("kind","交通");
        data.put("sellerName","销方名称");
        data.put("buyerName","购方名称");
        //发票代码
        data.put("invoiceCode","211001111013");
        //发票号码
        data.put("invoiceNum","87650531");
        //购方税号
        data.put("buyerTaxId","1234");
        //校验码
        data.put("checkCode","");
        //合计金额BigDecimal
        data.put("totalAmount",9);
        //报销状态
        data.put("purchaserStatus","");
        //买方税号
        data.put("sellerTaxId",12345);
        data.put("time","");

        return data;
    }
    /**
     * 增值税发票data
     *//*

    public static  Map<String, Object> OCR_SaveInvoiceData(){
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hasExist",false);
        data.put("hjje",5000);
        data.put("xsfMc","成都郫县希望职业学校");
        data.put("gmfNsrsbh","");
        data.put("gmfMc","吴星君计算机应用");
        //售方纳税人识别号
        data.put("xsfNsrsbh","52510124551090719U");
        //发票id
        data.put("pkInvoice","0");
        data.put("jshj","5600");
        //作废标志
        data.put("zfbz","N");
        //发票代码
        data.put("fpDm","211001111012");
        //发票类型
        data.put("fplx","9");
        //报销状态
        data.put("purchaserStatus",0);
        data.put("hasattache",false);
        //开票日期
        data.put("kprq","20171018");
        //发票号码
        data.put("fpHm","87650531");
        //校验码
        data.put("jym","557129");
        //征税方式
        data.put("zsfs","0");
        //表体明细行
        data.put("items",items());
        return data;
    }
    public static List<Object>items(){
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> item = new HashMap<String, Object>();
        //项目名称
        item.put("xmmc","服务费");
        //项目数量
        item.put("xmsl",1);
        //项目金额
        item.put("xmje","100");
        //税率
        item.put("sl",0.06);
        items.add(item);
        return items;
    }*/
    /**
     * 航空电子行程单DATA
     */
    public static  Map<String, Object> OCR_SaveAirData(){
        Map<String, Object> data = new HashMap<String, Object>();
        //开票日期"yyyyMMdd"
        data.put("date","20200610");
        data.put("id","20200610");
        data.put("fare",4444);

        data.put("agentCode","HKK068,08688003");
        data.put("issueBy","成都携程旅行社有限公司北京分社");
        //消费类型
        data.put("kind","交通");
        data.put("userName","www");
        data.put("userId","371102198004020527");
        data.put("caacDevelopFund",50);
        //校验码
        data.put("checkCode","");
        //合计金额BigDecimal
        data.put("totalAmount",9);
        data.put("ticketNum","8802175000276");
        //发票代码
        data.put("itemList",itemList());
        //发票号码
        data.put("fuelSurcharge",30);
        return data;
    }
   // 航空电子行程单DATA参数构造
    public static List<Object> itemList(){
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> item = new HashMap<String, Object>();
        //项目名称
        item.put("id","111");
        //项目数量
        item.put("airId","123");
        item.put("date","20200610");
        //项目金额
        item.put("seat","Y");
        //税率
        item.put("carrier","海航");
        item.put("from","杭州");
        item.put("time","19:23");
        item.put("to","北京");
        item.put("flightNumber","HU7678");


        items.add(item);
        return items;
    }

    /**
     * 火车票台账data
     */
    public static  Map<String, Object> OCR_SaveTrainData(){
        Map<String, Object> data = new HashMap<String, Object>();
        //开票日期"yyyyMMdd"
        data.put("date","20200610");
        data.put("id","20200610");
        //消费类型
        data.put("kind","交通");
        data.put("origin","北京南");
        data.put("destination","北京北");
        data.put("number","P026491");
        //合计金额BigDecimal
        data.put("totalAmount",9);
        data.put("trainNum","G9");
        data.put("name","XXX");
        data.put("time","12:00");
        data.put("seatNo","二等座");

        return data;
    }

    /**
     * 出租车保存报销data
     * @return
     */

    public static  Map<String, Object> OCR_SaveTaxiData(){
        Map<String, Object> data = new HashMap<String, Object>();
        //发票代码
        data.put("invoiceCode","211001111012");
        //发票号码
        data.put("invoiceNum","87650531");
        //开票日期"yyyyMMdd"
        data.put("date","20200610");
        data.put("id","20200610");
        //消费类型
        data.put("kind","交通");
        //合计金额BigDecimal
        data.put("totalAmount",9);
        //里程
        data.put("mileage",8);
        //上车时间
        data.put("startTime","13:23");
        //结束时间
        data.put("endTime","13:25");
        //发票所在地
        data.put("place","黑龙江省哈尔滨市");
        return data;
    }



    /**
     * 台账报销
     */
    public static Map<String,Object> reimbursed(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",reimbursedBills());
        return paramsMap;

    }
    /**
     * 台账报销参数构造
     */
    public static List<Object>  reimbursedBills(){
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        //发票代码
        data.put("invoiceCode","200006234694");
        //发票号码
        data.put("invoiceNum","55239602");
        data.put("billType","invoice");
        data.put("reimburseUser","XXX");
        datas.add(data);
        return datas;
    }
    /**
     * 台账取消报销
     */
    public static Map<String,Object> cancelReimbursed(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",reimbursedBills());
        return paramsMap;

    }
    /**
     * 台账记账
     */
    public  static Map<String, Object> account(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",accountBills());
        return paramsMap;
    }
    /**
     * 台账记账Bills
     */
    public  static List<Object> accountBills(){
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        //发票代码
        data.put("invoiceCode","012001900311");
        //发票号码
        data.put("invoiceNum","42195026");
        data.put("billType","invoice");
        data.put("accountUser","记账人");
        data.put("accountNote","记账人备注");
        datas.add(data);
        return datas;
    }
    //新增个人票夹
    public  static List<Object> addbills(){
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        //发票代码
        data.put("billType","invoice");
        //发票号码
        //data.put("imageId","1559122724325711872");
        data.put("imageContent",Base64Util.imageToBase64("D:\\常用文件\\税务云\\测试数据\\pdf\\卡尤迪医学检验实验室（北京）有限公司发票_20220323.pdf"));
        data.put("imageName","卡尤迪医学检验实验室（北京）有限公司发票_20220323.pdf");
        data.put("data",buildinvoices());
        datas.add(data);
        return datas;
    }
    //新增个人票夹参数构造
    private static Object buildData() {

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("date","20190101");
        data.put("invoiceCode", "111001881001");
        data.put("invoiceNum","09997919");
        data.put("kind", "交通");
        data.put("mileage", 7.5);
        data.put("place", "北京市");
        data.put("endTime","13:47");
        data.put("startTime", "13:33");
        data.put("totalAmount", 27.00);
        return data;
    }
    //新增个人票夹参数构造

    //      "srcBillCode" : "业务系统单据号",
    //      "srcapp":""
    //    },
    private static Object buildinvoices() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje",100);
        data.put("hjse",0);
        data.put("jshj",100);
        data.put("fpDm","222004522793");
        data.put("kprq","20220726");
        data.put("fpHm","42039025");
        data.put("jym","88535078414006107534");
        data.put("srcBillType","1243");
        data.put("srcBillCode", "51143619234234");
        //data.put("xsfMc", "卡尤迪医学检验实验室(北京)有限公司");
        //data.put("xsfNsrsbh", "91110114MA002DTF0G");
        //data.put("gmfNsrsbh", "911101081011288391");
        //data.put("gmfMc", "长城计算机软件与系统有限公司");
        //data.put("items",buildPiaoedaInvoices());
        return data;
    }

    private static Object buildPiaoedaInvoices() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> data2 = new HashMap<String, Object>();
        Map<String, Object> data1 = new HashMap<String, Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        //data.put("XMJSHJ", -10);
        data.put("XMMC", "*医疗服务*新型冠状病毒核酸检测费");
        data.put("XMBM", "202020012");
        data.put("XMSL", 1);
        data.put("xmje", 35);
        data.put("xmdj", 35);
        data.put("SE", 0);
        //税率16%需要写成0.16的格式
        data.put("SL", 0);
        items.add(data);
        return items;
    }

    //新增个人票夹参数构造 {

    //      "srcBillCode" : "业务系统单据号",
    //      "srcapp":""
    //    },
    private static Object buildSaveinvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("hjje",35);
        data.put("jshj",35);
        data.put("fpDm","011002200111");
        data.put("kprq","20220323");
        data.put("fpHm","18451369");
        data.put("jym","51282321600130265906");
        data.put("srcBillType","taxi");
        data.put("srcBillCode", "23456789");
        data.put("saveToken","41f45d10-a82b-4d9e-8705-53447edbf194");
        datas.add(data);
        return datas;
    }
    /**
     * 台账取消记账
     */
    public  static Map<String, Object> cancelAccount(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",accountBills());
        return paramsMap;
    }
    /**
     * 报销台账删除
     */
    public  static Map<String, Object> delete(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills",accountBills());
        return paramsMap;
    }
    //个人票夹新增
    public  static Map<String, Object> add(){

        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "18611143356");
        paramsMap.put("useremail", "");
        paramsMap.put("billList",addbills());
        return paramsMap;
    }

    //个人票夹删除
    public  static Map<String, Object> billDelete(){

        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "15011181852");
        paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("summarys",summarys());
        return paramsMap;
    }

    /**
     * 飞机票、火车票，出租车台账查询
     */
    public static Map<String,Object> find(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitDate_begin","2022-08-15");
        paramsMap.put("submitDate_end","2022-08-15");
        return paramsMap;
    }
    /**
     * 报销台账查询详情信息接口
     * */
    public static Map<String,Object> query(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("billCode", "3100204130");
        paramsMap.put("billNum","17059414");
        paramsMap.put("billType","invoice");
        return paramsMap;
    }
    /**
     * 个人票夹提交发票到报销台账_全票种
     * @return
     */
    public static Map<String,Object> commit(){
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "15011181852");
        paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("submitter", "提交人");
        paramsMap.put("srcBillCode", "23456789");
        paramsMap.put("srcBillType", "taxi");
        paramsMap.put("reimburseUser", "测试报销人1");
        paramsMap.put("returnEnclosure", "Y");
        paramsMap.put("busiOp", "1");
        paramsMap.put("summarys", summarys());
        return paramsMap;
    }
    public  static List<Object> summarys(){
    List<Object> datas = new ArrayList<Object>();
    Map<String, Object> data1 = new HashMap<String, Object>();
    Map<String, Object> data2 = new HashMap<String, Object>();
        data1.put("invoiceNum","64523810");
        data1.put("invoiceCode","011002200211");
        data1.put("purchaserStatus",33);
        data1.put("billType","invoice");
        data2.put("invoiceNum","51266661");
        data2.put("invoiceCode","111001881002");
        data2.put("billType","taxi");
        data2.put("purchaserStatus",1);
        datas.add(data1);
        //datas.add(data2);
      return datas;
    }
    //根据号码代码获取个人票夹信息参数构造
    public  static Map<String, Object> summary(){
        Map<String, Object> data1 = new HashMap<String, Object>();
        //png图片测试示例
        //data1.put("invoiceNum","68706393");
        //data1.put("invoiceCode","042001700107");
        //pdf测试示例
        //data1.put("invoiceNum","66311602");
        //data1.put("invoiceCode","011002000611");
        //图片为空示例
        data1.put("invoiceNum","68706393");
        data1.put("invoiceCode","042001700107");
        //data1.put("purchaserStatus",33);
        data1.put("billType","invoice");

        return data1;
    }
    //个人票夹行程单预览下载所需参数构造
    public  static Map<String, Object> summaryWithoutStatus(){
        Map<String, Object> data1 = new HashMap<String, Object>();
        data1.put("invoiceNum","68706393");
        data1.put("invoiceCode","042001700107");
        data1.put("billType","invoice");

        return data1;
    }
    //个人票夹列表查询参数构造
    public static Map<String, Object> buildInfo() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "13811111111");
        paramsMap.put("createtime_begin", "");
        paramsMap.put("createtime_end", "");
        paramsMap.put("date_begin", "");
        paramsMap.put("date_end", "");
        paramsMap.put("keyword", "");
        paramsMap.put("useremail", "");
        paramsMap.put("classification", "");
        //paramsMap.put("useremail", "wangyer@yonyou.com");
        //paramsMap.put("orgcode", "20160914001");
        //paramsMap.put("keyword", "68706393");
        return paramsMap;
    }
    //更改发票状态
    public static Map<String, Object> changFapiaoStatus() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "18611143356");
        //paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("summaries",summarys());
        return paramsMap;    }
    //根据号码代码获取信息
    public static Map<String, Object> summar() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "18611143356");
        //paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("summaries",summarys());
        return paramsMap;
    }
    //根据号码代码获取个人票夹信息
    public static Map<String, Object> detial() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "15011181852");
        paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("summary",summary());
        return paramsMap;
    }
    //个人票夹修改
    public static Map<String, Object> billUpdate() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "15011181852");
        paramsMap.put("invoiceNum","87650531");
        paramsMap.put("billType","train");
        paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("data",buildData());
        return paramsMap;
    }
    //个人票夹行程单预览下载所需参数
    public static Map<String, Object> view() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usermobile", "15011181852");
        paramsMap.put("useremail", "wangyer@yonyou.com");
        paramsMap.put("pagenum",1);
        paramsMap.put("summary",summaryWithoutStatus());
        return paramsMap;
    }

    /**
     * 全票种不查验进台账 构造参数
     */
    public static Map<String, Object> direct_save() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitter","xxx");
        paramsMap.put("srcBillCode","xxx");
        paramsMap.put("srcBillType","xxx");
        paramsMap.put("bills",buildBills());
        return paramsMap;
    }

    /**
     * 全票种不查验进台账bills集合 构造参数
     */
    private static List<Map<String, Object>> buildBills() {
        List<Map<String, Object>> bills = new ArrayList<>();
        Map<String, Object> bill = new HashMap<>();
        bill.put("billType","nontax");
        bill.put("data",buildTrainData());
        bills.add(bill);
        return bills;
    }

    /**
     * 全票种不查验进台账火车票data 构造参数
     */
    public static  Map<String, Object> buildTrainData(){
        Map<String, Object> data = new HashMap<String, Object>();
        //开票日期"yyyyMMdd"
        data.put("date","20200610");
        data.put("id","20200610");
        //消费类型
        data.put("kind","交通");
        data.put("origin","北京南");
        data.put("destination","北京北");
        data.put("number","P0264911");
        //合计金额BigDecimal
        data.put("totalAmount",9);
        data.put("trainNum","G9");
        data.put("name","XXX");
        data.put("time","12:00");
        data.put("seatNo","二等座");

        return data;
    }

    /**
     * 报销台账发票上传返回全票面信息(PDF) 参数构造
     */
    public static Map<String, Object> V4_UPLOADPDF() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("usercode", "201609140000001");
        paramsMap.put("useremail", "20160914001");
        paramsMap.put("usermobile","xxx");
        paramsMap.put("nsrsbh","91440400192531091M");
        paramsMap.put("orgcode","");
        paramsMap.put("pdfFiles",buildPdfFiles());
        return paramsMap;
    }

    /**
     *
     */
    private static List<Map<String, Object>> buildPdfFiles() {
        List<Map<String, Object>> pdfFiles = new ArrayList<>();
        Map<String, Object> pdfFile = new HashMap<>();
        pdfFile.put("srcBillType","");
        pdfFile.put("srcBillCode","");
        pdfFile.put("classification","通讯");
        pdfFile.put("fileName","增值税电子发票.pdf");
        pdfFile.put("content", Base64Util.imageToBase64("D:\\常用文件\\税务云\\测试数据\\pdf\\增值税电子发票.pdf"));
        pdfFile.put("classification","餐饮");
        pdfFiles.add(pdfFile);
        return pdfFiles;
    }

    /**
     * 功能描述: 纸票查验进报销台账 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/20 17:24
     */
    public static Map<String, Object> VERIFY_AND_SUBMIT() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitter", "kw");
        paramsMap.put("taxRateExchange", 1);
        paramsMap.put("invoices", buildPaperInvoices());
        return paramsMap;
    }

    /**
     * 功能描述: 纸票查验进报销台账明细 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/20 17:28
     */
    private static Object buildPaperInvoices() {
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("fpDm","011002100711");
        data.put("fpHm","58141396");
        data.put("kprq","20220226");
        data.put("hjje",198.02);
        data.put("jshj",200.00);
        data.put("jym","07635602510068606352");
        data.put("srcapp","");
        data.put("srcBillType","invoice");
        data.put("srcBillCode", "51143619234234");
        data.put("imageId", "");
        data.put("numberConfirm", "");
        return data;
    }

    /**
     * 功能描述:  不需要查验进入台账 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/20 17:35
     */
    public static Map<String, Object> REIMBURSE_DIRECT_SAVE() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitter", "kw");
        paramsMap.put("invoices", buildNoVerifyInvoices());
        return paramsMap;
    }

    /**
     * 功能描述: 不需要查验进入台账发票 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/20 17:41
     */
    private static Object buildNoVerifyInvoices() {
        List<Object> datas = new ArrayList<Object>();
        Map<String, Object> data = new HashMap<String, Object>();
        data.put("thirdVerifyStatus", "1");
        data.put("hjje", 1395.28);
        data.put("jshj", 1479.00);
        data.put("fpDm", "3100201130");
        data.put("kprq", "20210421");
        data.put("fpHm", "51143619");
        data.put("jym", "162191");
        data.put("srcBillType", "invoice");
        data.put("srcBillCode", "51143619");
        data.put("fplx", "4");
        data.put("gmfDzdh", "广州市黄埔区黄埔大道东856号（A-2）801-812室（仅限办公） 02062981357");
        data.put("gmfMc", "广东卓志供应链科技有限公司");
        data.put("gmfNsrsbh", "91440101080359573F");
        data.put("gmfYhzh", "中国银行股份有限公司广州港湾路支行 678261875314");
        data.put("xsfDzdh", "162191");
        data.put("xsfYhzh", "工商银行上海市虹口区武进路支行1001213909200188728");
        data.put("xsfMc", "上海潼季酒店管理有限公司");
        data.put("xsfNsrsbh", "91310109324291588H");
        data.put("items", buildNoVerifyItems());
        datas.add(data);
        return datas;
    }

    /**
     * 功能描述: 不需要查验进入台账明细 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/20 17:42
     */
    private static Object buildNoVerifyItems() {
        List<Object> items = new ArrayList<Object>();
        Map<String, Object> item = new HashMap<String, Object>();
        //   "detailMotor":null,
        item.put("dw", "天");
        item.put("yhzcbs", 0);
        item.put("xmsl", 3);
        item.put("xmmc", "*住宿服务*住宿费");
        item.put("xmje", 1395.28);
        item.put("xmdj", 465.0933333333);
        item.put("sl", 0.060000);
        item.put("se", 83.72);
        item.put("flightNumber", "HU7678");
        items.add(item);
        return items;
    }

    /**
     * 功能描述: 报销台账查询附件(全票种) 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:32
     */
    public static Map<String, Object> VIEW_URL() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills", buildViewUrlBills());
        return paramsMap;
    }

    /**
     * 功能描述: 报销台账查询附件(全票种) 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:34
     */
    private static Object buildViewUrlBills() {
        List<Object> bills = new ArrayList<Object>();
        Map<String, Object> bill = new HashMap<String, Object>();
        bill.put("billType", "invoice");
        bill.put("invoiceNum", "20142476");
        bill.put("invoiceCode", "222006680862");
        bills.add(bill);
        return bills;
    }

    /**
     * 功能描述: 其他发票台账查询 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:38
     */
    public static Map<String, Object> OTHER() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("submitDate_begin", "2000-07-05");
        paramsMap.put("submitDate_end", "2019-07-09");
        return paramsMap;
    }

    /**
     * 功能描述: 报销台账更新凭证号接口
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:58
     */
    public static Map<String, Object> UPDATE_VOUCHERID() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills", buildUpdateVoucheridBills());
        return paramsMap;
    }

    private static Object buildUpdateVoucheridBills() {
        List<Object> bills = new ArrayList<Object>();
        Map<String, Object> bill = new HashMap<String, Object>();
        bill.put("invoiceCode", "044002005111");
        bill.put("invoiceNum", "13950494");
        bill.put("billType", "invoice");
        bill.put("srcBillType", "222222222222");
        bill.put("srcBillCode", "222222222222");
        bill.put("voucherid", "222222222222");
        bills.add(bill);
        return bills;
    }

    /**
     * 功能描述: 报销台账更新来源单据号接口 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:53
     */
    public static Map<String, Object> UPDATE_SRCBILL() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("bills", buildUpdateSrcbillBills());
        return paramsMap;
    }

    private static Object buildUpdateSrcbillBills() {
        List<Object> bills = new ArrayList<Object>();
        Map<String, Object> bill = new HashMap<String, Object>();
        bill.put("invoiceCode", "044002005111");
        bill.put("invoiceNum", "13950494");
        bill.put("billType", "invoice");
        bill.put("srcBillType", "222222222222");
        bill.put("srcBillCode", "222222222222");
        bills.add(bill);
        return bills;
    }

    /**
     * 功能描述: 报销台账设置项目 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 9:57
     */
    public static Map<String, Object> PROJECT_UPDATE() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("nsrsbh", "201609140000001");
        paramsMap.put("orgcode", "20160914001");
        paramsMap.put("projectCode", "jxx03");
        paramsMap.put("bSubInvoice", "N");
        paramsMap.put("collectionProjectParams", buildProjectUpdateBills());
        return paramsMap;
    }

    private static Object buildProjectUpdateBills() {
        List<Object> bills = new ArrayList<Object>();
        Map<String, Object> bill = new HashMap<String, Object>();
        bill.put("fpHm", "13950494");
        bill.put("fpDm", "044002005111");
        bill.put("billType", "invoice");
        bills.add(bill);
        return bills;
    }

    /**
     * 功能描述:  报销台账置支付状态接口 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 10:09
     */
    public static Map<String, Object> paid() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("billCode", "044002005111");
        paramsMap.put("billNum", "13950494");
        paramsMap.put("billType", "invoice");
        return paramsMap;
    }

    /**
     * 功能描述: 报销台账置取消支付状态接口 构造参数
     * @Author: jiaoguojin
     * @Date: 2022/6/21 10:09
     */
    public static Map<String, Object> canclePaid() {
        Map<String, Object> paramsMap = new HashMap<String, Object>();
        paramsMap.put("billCode", "044002005111");
        paramsMap.put("billNum", "13950494");
        paramsMap.put("billType", "invoice");
        return paramsMap;
    }
}

