### 使用说明

#### 执行方式
直接执行java.demo.test包下的测试类的main方法即可测试

#### 切换环境
切换环境测试需要修改URLConfigEnum.java的参数 KEY_PATH PASSWORD APPID DOMAIN

#### 包含义
- crypto 放置密钥相关的处理类
- entity 放置参数构建类
- test 放置接口测试的入口类
- utils 放置请求相关的工具类
- test\unuploadApi放置了openApi不存在的测试接口

######         test\output_invoice对应[税务云OpenAPI](https://fapiao.yonyoucloud.com/apidoc/) 下的销项开票测试类

![销项开票接口图](.\pic\销项开票接口图.png)

######  test\personal_ticket_holder对应[税务云OpenAPI](https://fapiao.yonyoucloud.com/apidoc/) 下的个人票夹测试类 与openApi目录相对应

###### ![20210518112221](.\pic\20210518112221.png)

######        test\input_ticket对应[税务云OpenAPI](https://fapiao.yonyoucloud.com/apidoc/) 下的进程受票测试类

![进项受票接口图](.\pic\进项受票接口图.png)

######  test\ofd对应[税务云OpenAPI](https://fapiao.yonyoucloud.com/apidoc/) 下的ofd能力
![ofd接口图](.\pic\ofd接口图.png)

#### 证书
- resources/certificate/pro22.pfx

#### 线下API文档
- resources/certificate/电子发票OpenAPI文档V1.24.html
- resources/certificate/全票种-直接对接台账（ocr方式，不通过个人票夹）.pdf
- resources/certificate/电子发票报销应用OpenAPI第2版(包含电票上传).html
## 说明

##### 线上api接口对应test下的四个包 与线上api相对应

21/06/01更新

线上受票下的电子发票采购台账（demo.test.input_ticket.PurchaseLedgerTest）OpenAPI新增接口 

1. 附件预览 
2. 不需要查验进入台账
3. 采购台账更新凭证号接口
4. 采购台账更新来源单据号接口

22/06/21更新

整理接口调用示例，将所有线上的接口都按目录分到了不同的包下，详情见包含义

##### 线下api接口 说明 线上未上传的api放在unuploadApi包内 包含两个部分

input_ticket对应未上传的受票接口

1. ```
   PaperTicketInspection类 对应 纸票查验保存采购台帐
   ```

2. ```
   PaperTicketToSalesAccount类对应 纸票查验保存报销台帐
   ```

other对应未上传的其他接口

1. Contract类对应合同管理Api接口测试入口
2. SaveInvoiceQuery对应销项信息查询
3. Synchronization对应用户信息同步接口
4. TaxRelatedProject对应涉税项目管理接口
5. WecheatDocking对应微信企业对接接口

output_invoice对应未上传的开票接口

1. AutoReplenishment对应发票自动补充接口
2. RedInvoice对应专票红冲接口

#### InvoiceBuildParam 放置销项开票中 开票申请所需参数方法

- buildTaxPrintParam() 构造发票打印数据
- buildInsertWithArrayPostParam() 构造开票蓝票请求服务 表单数据
- issue() 开票申请审核通过
- red() 发票红冲请求服务及电子发票部分红冲
- insertWithSplit()   开票蓝票请求服务--发票拆分
- buildUrlConfigs()  url回掉配置
- buildSmsConfigs()  构造短信发送信息
- buildRequestDatas() 构造requestdatas
- redRequestDatas()  构造redRequestdatas
- buildRequestDatasSplit()  构造蓝票请求服务--发票拆分数据
- buildItems() 构造request发票明细
- buildFpqqlsh() 获取发票请求流水号
长度不超过20位，长度在1到20位的字母和数字组合，不可以重复的，
不要包含window系统文件名限制的特殊字符


#### StaBookBuildParam.java放置进程受票中 报销台账全票种参数

- buildRecognisePostParam()  OCR识别
-  OCR_Save() 识别结果保存台帐
- OCR_SaveBills() 保存报销票据明细
- OCR_SaveMachineData() 机打发票data
- OCR_SaveAirData() 航空电子行程单DATA
- itemList() 航空电子行程单DATA里面itemList参数设置
- OCR_SaveTrainData() 火车票台账data
- OCR_SaveTaxiData() 出租车保存报销data
- reimbursed() 台账报销
-  reimbursedBills() 台账报销Bills参数设置
- cancelReimbursed() 台账取消报销
- account() 台账记账
- accountBills()台账记账Bills
- cancelAccount() 台账取消记账
- delete() 报销台账删除
- find()   飞机票、火车票，出租车台账查询
- commit() 个人票夹提交发票到报销台账_全票种
- summary() 个人票夹提交发票到报销台账_全票种中summary参数设置

#### ReimburseCollection.java放置个人票夹中 个人全票种中





#####  demo.test.unuploadApi.output_invoice.AutoReplenishment  新增自动补全测试接口





#### 应用参数

-  uploadpdf()  发票上传与发票上传V2，V2返回值增加疑票状态
-  pdfFiles() uploadpdf中pdfFile的参数配置
-  CollDelete() 发票取消上传参数设置
-  CollReimbursed() 发票已报销
-  CollUnreimbursed() 发票取消报销
-  query() 报销台账查询接口（新）
-  accountStatus()   入账
-  CollCancelAccount() 取消入账
-  invoices() 为CollDelete()、CollReimbursed()、CollUnreimbursed()、accountStatus()、 
CollCancelAccount()类中invoices参数进行设置